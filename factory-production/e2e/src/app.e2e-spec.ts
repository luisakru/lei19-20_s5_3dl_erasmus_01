'use strict'; // necessary for es6 output in node

import { browser, element, by, ElementFinder, ElementArrayFinder } from 'protractor';
import { promise } from 'selenium-webdriver';

const expectedH1 = 'FactoryProduction';
const expectedTitle = `${expectedH1}`;
const targetOperation = { id: 1, duration: 20, machineTypeIds: 3 };
const targetOperationMDFIndex = 3;
const durationSuffix = 'X';
const newOperationDuration = targetOperation.duration + durationSuffix;


class Operation {
  id: number;
  duration: number;
  machineTypeIds: number;

  // Factory methods

  // Operation from string formatted as '<id> <duration> <machineTypeIds>'.
  static fromString(s: string): Operation {
    return {
      id: +s.substr(0, s.indexOf(' ')),
      duration: +s.substr(0, s.indexOf(' ') + 1),
      machineTypeIds: +s.substr(0, s.indexOf(' ')+ 2 ),
    };
  }

  // Operation from hero list <li> element.
  static async fromLi(li: ElementFinder): Promise<Operation> {
      let stringsFromA = await li.all(by.css('a')).getText();
      let strings = stringsFromA[0].split(' ');
      return { id: +strings[0], duration: +strings[1], machineTypeIds: +strings[2]};
  }

  // Operation id and name from the given detail element.
  static async fromDetail(detail: ElementFinder): Promise<Operation> {
    // Get operation id from the first <div>
    let _id = await detail.all(by.css('div')).first().getText();
    // Get duration from the first <div>
    let _duration = await detail.all(by.css('div')).first().getText();
     // Get machineTypeIds from the first <div>
     let _machineTypeIds = await detail.all(by.css('div')).first().getText();
    return {
        id: +_id.substr(_id.indexOf(' ') + 1),
        duration: +_duration.substr(_id.indexOf(' ') + 2),
        machineTypeIds: +_machineTypeIds.substr(_id.indexOf(' ') + 3),
        
    };
  }

}

class Product {
  id: number;
  manufactoringPlanId: number;

  // Factory methods

  // Product from string formatted as '<id>'.
  static fromString(s: string): Product {
    return {
      id: +s.substr(0, s.indexOf(' ')),
      manufactoringPlanId: +s.substr(0, s.indexOf(' ') + 1),
    };
  }

  // Product from hero list <li> element.
  static async fromLi(li: ElementFinder): Promise<Product> {
      let stringsFromA = await li.all(by.css('a')).getText();
      let strings = stringsFromA[0].split(' ');
      return { id: +strings[0], manufactoringPlanId: +strings[1]};
  }

  // Product id and name from the given detail element.
  static async fromDetail(detail: ElementFinder): Promise<Product> {
    // Get product id from the first <div>
    let _id = await detail.all(by.css('div')).first().getText();
    // Get duration from the first <div>
    let _manufactoringPlanId = await detail.all(by.css('div')).first().getText();
    
    return {
        id: +_id.substr(_id.indexOf(' ') + 1),
        manufactoringPlanId: +_manufactoringPlanId.substr(_id.indexOf(' ') + 2), 
    };
  }

}

describe(' testing ', () => {

  beforeAll(() => browser.get(''));

  function getPageElts() {
    let navElts = element.all(by.css('app-root nav a'));

    return {
      navElts: navElts,

      appMDFHref: navElts.get(0),
      appMDF: element(by.css('app-root app-mdf')),
      allElementsMDF: element.all(by.css('app-root app-mdf li')),
      selectedMDFSubview: element(by.css('app-root app-mdf > div:last-child')),

      appMDPHref: navElts.get(1),
      appMDP: element(by.css('app-root app-mdp')),
      allElementsMDP: element.all(by.css('app-root app-mdp li')),
      selectedMDPSubview: element(by.css('app-root app-mdp > div:last-child')),

      operationDetail: element(by.css('app-root app-operation-detail > div')),
      machineDetail: element(by.css('app-root app-machine-detail > div')),
      machinetypeDetail: element(by.css('app-root app-machinetype-detail > div')),
    };
  }


  describe('Initial page', () => {

    it(`has title '${expectedTitle}'`, () => {
      expect(browser.getTitle()).toEqual(expectedTitle);
    });

    const expectedViewNames = ['MDF', 'MDP'];
    it(`has views ${expectedViewNames}`, () => {
      let viewNames = getPageElts().navElts.map((el: ElementFinder) => el.getText());
      expect(viewNames).toEqual(expectedViewNames);
    });

  });


  describe('MDF:  ', () => {

    beforeAll(() => browser.get(''));
    browser.sleep(1000);

    it('can switch to MDF view', () => {
      getPageElts().appMDFHref.click();
      let page = getPageElts();
      expect(page.appMDF.isPresent()).toBeTruthy();
      //expect(page.allElementsMDF.count()).toEqual(28, 'number of elements in MDF');
    });

    it('can route to operation details', async () => {
      getOperationLiEltById(targetOperation.id).click();

      let page = getPageElts();
      expect(page.operationDetail.isPresent()).toBeTruthy('shows operation detail');
      let operation = await Operation.fromDetail(page.operationDetail);
      expect(operation.id).toEqual(targetOperation.id);
    });


    it(`adds an operation `, async () => {
      const newOperationDuration = 30;
      const newOperationMachinetypes = 3;
      const operationsBefore = await toOperationArray(getPageElts().allElementsMDF);
      const numOperations = operationsBefore.length;

      element(by.css('#input')).sendKeys(newOperationDuration);
      element(by.css('input')).sendKeys(newOperationMachinetypes);
      element(by.css('button.#operationAdd')).click();

      let page = getPageElts();
      let operationsAfter = await toOperationArray(page.allElementsMDF);
      expect(operationsAfter.length).toEqual(numOperations + 1, 'number of operations');

    });

  });


    describe('MDP:  ', () => {

      beforeAll(() => browser.get(''));
      browser.sleep(1000);
  
      it('can switch to MDP view', () => {
        getPageElts().appMDPHref.click();
        let page = getPageElts();
        expect(page.appMDP.isPresent()).toBeTruthy();
        //expect(page.allElementsMDF.count()).toEqual(2, 'number of elements in MDP');
      });


      it(`adds a product manufacturingPlanId: `, async () => {
        const newProductManuPlan = 2;
        const productsBefore = await toProductArray(getPageElts().allElementsMDP);
        const numProducts = productsBefore.length;
  
        element(by.css('input')).sendKeys(newProductManuPlan);
        element(by.css('button')).click();
  
        let page = getPageElts();
        let productsAfter = await toProductArray(page.allElementsMDP);
        expect(productsAfter.length).toEqual(numProducts + 1, 'number of products');
      });

   

    

    // it(`updates hero name (${newHeroName}) in details view`, updateHeroNameInDetailView);

    // it(`shows ${newHeroName} in Heroes list`, () => {
    //   element(by.buttonText('save')).click();
    //   browser.waitForAngular();
    //   let expectedText = `${targetHero.id} ${newHeroName}`;
    //   expect(getHeroAEltById(targetHero.id).getText()).toEqual(expectedText);
    // });

    // it(`deletes ${newHeroName} from Heroes list`, async () => {
    //   const heroesBefore = await toHeroArray(getPageElts().allHeroes);
    //   const li = getHeroLiEltById(targetHero.id);
    //   li.element(by.buttonText('x')).click();

    //   const page = getPageElts();
    //   expect(page.appHeroes.isPresent()).toBeTruthy();
    //   expect(page.allHeroes.count()).toEqual(9, 'number of heroes');
    //   const heroesAfter = await toHeroArray(page.allHeroes);
    //   // console.log(await Hero.fromLi(page.allHeroes[0]));
    //   const expectedHeroes =  heroesBefore.filter(h => h.name !== newHeroName);
    //   expect(heroesAfter).toEqual(expectedHeroes);
    //   // expect(page.selectedHeroSubview.isPresent()).toBeFalsy();
    // });

    // it(`adds back ${targetHero.name}`, async () => {
    //   const newHeroName = 'Alice';
    //   const heroesBefore = await toHeroArray(getPageElts().allHeroes);
    //   const numHeroes = heroesBefore.length;

    //   element(by.css('input')).sendKeys(newHeroName);
    //   element(by.buttonText('add')).click();

    //   let page = getPageElts();
    //   let heroesAfter = await toHeroArray(page.allHeroes);
    //   expect(heroesAfter.length).toEqual(numHeroes + 1, 'number of heroes');

    //   expect(heroesAfter.slice(0, numHeroes)).toEqual(heroesBefore, 'Old heroes are still there');

    //   const maxId = heroesBefore[heroesBefore.length - 1].id;
    //   expect(heroesAfter[numHeroes]).toEqual({id: maxId + 1, name: newHeroName});
    // });

  });







  // describe('Dashboard tests', () => {

  //   beforeAll(() => browser.get(''));

  //   it('has top heroes', () => {
  //     let page = getPageElts();
  //     expect(page.topHeroes.count()).toEqual(4);
  //   });

  //   it(`selects and routes to ${targetHero.name} details`, dashboardSelectTargetHero);

  //   it(`updates hero name (${newHeroName}) in details view`, updateHeroNameInDetailView);

  //   it(`cancels and shows ${targetHero.name} in Dashboard`, () => {
  //     element(by.buttonText('go back')).click();
  //     browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

  //     let targetHeroElt = getPageElts().topHeroes.get(targetHeroDashboardIndex);
  //     expect(targetHeroElt.getText()).toEqual(targetHero.name);
  //   });

  //   it(`selects and routes to ${targetHero.name} details`, dashboardSelectTargetHero);

  //   it(`updates hero name (${newHeroName}) in details view`, updateHeroNameInDetailView);

  //   it(`saves and shows ${newHeroName} in Dashboard`, () => {
  //     element(by.buttonText('save')).click();
  //     browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

  //     let targetHeroElt = getPageElts().topHeroes.get(targetHeroDashboardIndex);
  //     expect(targetHeroElt.getText()).toEqual(newHeroName);
  //   });

  });




//   async function mdfSelectTargetOperation() {
//     let targetHeroElt = getPageElts().topHeroes.get(targetHeroDashboardIndex);
//     expect(targetHeroElt.getText()).toEqual(targetHero.name);
//     targetHeroElt.click();
//     browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

//     let page = getPageElts();
//     expect(page.heroDetail.isPresent()).toBeTruthy('shows hero detail');
//     let hero = await Hero.fromDetail(page.heroDetail);
//     expect(hero.id).toEqual(targetHero.id);
//     expect(hero.name).toEqual(targetHero.name.toUpperCase());
//   }

//   async function updateHeroNameInDetailView() {
//     // Assumes that the current view is the hero details view.
//     addToHeroName(nameSuffix);

//     let page = getPageElts();
//     let hero = await Hero.fromDetail(page.heroDetail);
//     expect(hero.id).toEqual(targetHero.id);
//     expect(hero.name).toEqual(newHeroName.toUpperCase());
//   }

// });




function expectHeading(hLevel: number, expectedText: string): void {
    let hTag = `h${hLevel}`;
    let hText = element(by.css(hTag)).getText();
    expect(hText).toEqual(expectedText, hTag);
};

function getOperationLiEltById(id: number): ElementFinder {
  let spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('..'));
}

async function toOperationArray(allElements: ElementArrayFinder): Promise<Operation[]> {
  let promisedOperations = await allElements.map(Operation.fromLi);
  // The cast is necessary to get around issuing with the signature of Promise.all()
  return <Promise<any>> Promise.all(promisedOperations);
}

async function toProductArray(allElements: ElementArrayFinder): Promise<Product[]> {
  let promisedProducts = await allElements.map(Product.fromLi);
  // The cast is necessary to get around issuing with the signature of Promise.all()
  return <Promise<any>> Promise.all(promisedProducts);
}



