export class Order {
    _id: string;
    status: string;
    quantity: number;
    customer?: string;
    product?: number;
    timestamp: string;
    changed: string;
    deliveryDay: string;
    //order: Codes;
}