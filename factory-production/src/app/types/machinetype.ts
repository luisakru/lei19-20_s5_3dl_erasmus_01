export class MachineType {
  id: number;
  description: string;
  operationsIds: number[];
}