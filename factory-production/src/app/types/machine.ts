export class Machine {
  id: number;
  position: number;
  machineTypeId: number;
  isActive: boolean;
}