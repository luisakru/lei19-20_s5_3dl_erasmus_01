export class Operation {
  id: number;
  duration: number;
  machineTypeIds: [number];
}
