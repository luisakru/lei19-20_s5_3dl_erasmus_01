import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Operation } from '../../../types/operation';
import { OperationService } from '../../../services/operation/operation.service';

@Component({
  selector: 'app-operation-detail',
  templateUrl: './operation-detail.component.html',
  styleUrls: ['./operation-detail.component.css']
})
export class OperationDetailComponent implements OnInit {
  [x: string]: any;
  @Input() operation: Operation;

  constructor(
    private route: ActivatedRoute,
    private operationService: OperationService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getOperation();
  }

  getOperation(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.operationService.getOperation(id)
      .subscribe(operation => this.operation = operation);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.operationService.updateOperation(this.operation)
      .subscribe(() => this.goBack());
  }
}

