import { Component, OnInit } from '@angular/core';
import { Operation } from 'src/app/types/operation';
import { OperationService } from 'src/app/services/operation/operation.service';

@Component({
  selector: 'operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {

  operations: Operation[];
  error: string;

  constructor(private operationService: OperationService) { }

  ngOnInit() {
    this.getOperations();
    this.error = null;
  }

  getOperations(): void {
    this.operationService.getOperations().subscribe(mdf => this.operations = mdf);
  }

  addOperation(duration: number, machineTypeIdsInput: string): void {
    if (!duration) {
      this.error = "Duration cannot be empty"
      return;
    }

    machineTypeIdsInput = machineTypeIdsInput.trim();
    const machineTypeIds = machineTypeIdsInput.split(" ").map(Number);

    if (!machineTypeIds || machineTypeIds.length === 0 || machineTypeIdsInput === "" || machineTypeIds.includes(NaN)) {
      this.error = "Operation needs to have at least one machine type. "
      return;
    }

    this.operationService.addOperation({ duration, machineTypeIds } as Operation).subscribe(operation => {
      if (operation) {
        this.operations.push(operation);
        this.error = "";
      } else {
        this.error = "Failed to add a new operation";
      }
    });
  }
}
