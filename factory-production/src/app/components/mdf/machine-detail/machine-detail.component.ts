import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Machine } from '../../../types/machine';
import { MachineService } from '../../../services/machine/machine.service';
import { MachineType } from 'src/app/types/machinetype';


@Component({
  selector: 'app-machine-detail',
  templateUrl: './machine-detail.component.html',
  styleUrls: ['./machine-detail.component.css']
})
export class MachineDetailComponent implements OnInit {
  [x: string]: any;
  machine: Machine;
  machineToUpdate: Machine;

  constructor(
    private route: ActivatedRoute,
    private machineService: MachineService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getMachine();
  }

  getMachine(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.machineService.getMachine(id)
      .subscribe(machine => {
        this.machine = machine
        this.machineToUpdate = Object.assign({}, machine);
      });
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.machineService.updateMachineTypeofMachine(this.machineToUpdate)
      .subscribe(() => this.goBack());
  }

  turnOn(): void {
    this.machineToUpdate.isActive = true;
    this.machineService.updateMachineTypeofMachine(this.machineToUpdate)
      .subscribe(() => this.getMachine());
  }

  turnOff(): void {
    this.machineToUpdate.isActive = false;
    this.machineService.updateMachineTypeofMachine(this.machineToUpdate)
      .subscribe(() => this.getMachine());
  }

}

