import { Component, OnInit } from '@angular/core';
import { ProductionLine } from 'src/app/types/productionLine';
import { ProductionlineService } from 'src/app/services/productionline/productionline.service';

@Component({
  selector: 'productionline',
  templateUrl: './productionlines.component.html',
  styleUrls: ['./productionlines.component.css']
})
export class ProductionlineComponent implements OnInit {

  productionLines: ProductionLine[];
  error: string;

  constructor(private productionLineService: ProductionlineService) { }

  ngOnInit() {
    this.getProductionLines();
    this.error = null;
  }

  getProductionLines(): void {
    this.productionLineService.getProductionLines().subscribe(pl => this.productionLines = pl);
  }

  addProductionLine(machineIdsString: string): void {
    machineIdsString = machineIdsString.trim();
    const machinesIds = machineIdsString.split(" ").map(Number);

    if (machineIdsString === "" || machinesIds.includes(NaN)) {
      this.error = "Production line should have at least one machine."
      return;
    }

    this.productionLineService.addProductionLine({ machinesIds } as ProductionLine).subscribe(productionLine => {
      if (productionLine) {
        this.productionLines.push(productionLine);
        this.error = null;
      } else {
        this.error = "Failed to add a new production line";
      }
    });
  }
}
