import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionlineDetailComponent } from './productionline-detail.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http'; 

describe('ProductionlineDetailComponent', () => {
  let component: ProductionlineDetailComponent;
  let fixture: ComponentFixture<ProductionlineDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [ ProductionlineDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionlineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
