import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ProductionLine } from 'src/app/types/productionLine';
import { ProductionlineService } from 'src/app/services/productionline/productionline.service';

@Component({
  selector: 'app-productionline-detail',
  templateUrl: './productionline-detail.component.html',
  styleUrls: ['./productionline-detail.component.css']
})
export class ProductionlineDetailComponent implements OnInit {
  [x: string]: any;
  @Input() productionLine: ProductionLine;

  constructor(
    private route: ActivatedRoute,
    private productionlineService: ProductionlineService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getProductionLine();
  }

  getProductionLine(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    const a = this.productionlineService.getProductionLine(id)
      .subscribe(pl => this.productionLine = pl);            
  }


  goBack(): void {
    this.location.back();
  }

}
