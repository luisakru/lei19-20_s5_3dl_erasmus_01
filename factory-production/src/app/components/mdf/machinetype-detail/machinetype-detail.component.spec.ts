import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinetypeDetailComponent } from './machinetype-detail.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule } from '@angular/forms';

describe('MachinetypeDetailComponent', () => {
  let component: MachinetypeDetailComponent;
  let fixture: ComponentFixture<MachinetypeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, FormsModule],
      declarations: [ MachinetypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinetypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
