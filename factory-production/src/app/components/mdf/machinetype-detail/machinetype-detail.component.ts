import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MachineType } from '../../../types/machinetype';
import { MachineTypeService } from '../../../services/machinetype/machinetype.service';
import { Operation } from 'src/app/types/operation';
import { Machine } from 'src/app/types/machine';
import { OperationsList } from 'src/app/types/OperationsList';

@Component({
  selector: 'app-machinetype-detail',
  templateUrl: './machinetype-detail.component.html',
  styleUrls: ['./machinetype-detail.component.css']
})
export class MachinetypeDetailComponent implements OnInit {
  [x: string]: any;
  machinetype: MachineType;
  machinetypeOperations: Operation[];
  machinetypeMachines: Machine[];

  constructor(
    private route: ActivatedRoute,
    private machineTypeService: MachineTypeService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getMachineType();
    this.getMachineTypeOperations();
    this.getMachineTypeMachines();
  }

  getMachineType(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.machineTypeService.getMachineType(id)
      .subscribe(machinetype => this.machinetype = machinetype);
  }

  getMachineTypeOperations(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.machineTypeService.getMachineTypeOperations(id)
      .subscribe(machinetypeOperations => {
        if (machinetypeOperations.length === 0) {
          console.log("error");
        } else {
          this.machinetypeOperations = machinetypeOperations
        }
      });
  }

  getMachineTypeMachines(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.machineTypeService.getMachineTypeMachines(id)
      .subscribe(machinetypeMachines => {
        this.machinetypeMachines = machinetypeMachines
      });
  }

  goBack(): void {
    this.location.back();
  }

  save(operationsIdsString: string): void {
    operationsIdsString = operationsIdsString.trim();
    const ids = operationsIdsString ? operationsIdsString.split(" ").map(Number) : [];

    this.machineTypeService.updateMachineType(this.machinetype, { ids } as OperationsList)
      .subscribe(() => this.goBack());
  }
}

