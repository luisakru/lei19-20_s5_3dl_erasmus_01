import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MdfComponent } from './mdf.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('MdfComponent', () => {
  let component: MdfComponent;
  let fixture: ComponentFixture<MdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MdfComponent ],
      schemas: [NO_ERRORS_SCHEMA]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
