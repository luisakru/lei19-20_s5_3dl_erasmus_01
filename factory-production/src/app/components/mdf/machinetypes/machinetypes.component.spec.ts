import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinetypesComponent } from './machinetypes.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http'; 

describe('MachinetypesComponent', () => {
  let component: MachinetypesComponent;
  let fixture: ComponentFixture<MachinetypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [ MachinetypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinetypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
