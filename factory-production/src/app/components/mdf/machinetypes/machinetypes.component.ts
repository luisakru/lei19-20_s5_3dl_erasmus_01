import { Component, OnInit } from '@angular/core';
import { MachineTypeService } from 'src/app/services/machinetype/machinetype.service';
import { MachineType } from 'src/app/types/machinetype';

@Component({
  selector: 'machinetypes',
  templateUrl: './machinetypes.component.html',
  styleUrls: ['./machinetypes.component.css']
})
export class MachinetypesComponent implements OnInit {

  machinetypes: MachineType[];
  error: string;

  constructor(private machineTypeService: MachineTypeService) { }

  ngOnInit() {
    this.getMachineTypes();
    this.error = null;
  }

  getMachineTypes(): void {
    this.machineTypeService.getMachineTypes().subscribe(mdf => this.machinetypes = mdf);
  }

  addMachineType(description: string, operationsIdInput: string): void {
    description = description.trim();
    operationsIdInput = operationsIdInput.trim();

    if (!description) {
      this.error = "Description cannot be empty."
      return;
    }

    const operationsIds = operationsIdInput ? operationsIdInput.split(" ").map(Number) : [];
    if (operationsIds.includes(NaN)) {
      this.error = "Incorrect operations ids"
      return;
    }

    this.machineTypeService.addMachineType({ description, operationsIds } as MachineType)
      .subscribe(machinetype => {
        if (machinetype) {
          this.machinetypes.push(machinetype);
          this.error = null;
        } else {
          this.error = "Failed to add a new machine type";
        }
      });
  }
}
