import { Component, OnInit } from '@angular/core';
import { MachineService } from 'src/app/services/machine/machine.service';
import { Machine } from 'src/app/types/machine';

@Component({
  selector: 'machines',
  templateUrl: './machines.component.html',
  styleUrls: ['./machines.component.css']
})
export class MachinesComponent implements OnInit {

  machines: Machine[];
  error: string;

  constructor(private machineService: MachineService) { }

  ngOnInit() {
    this.getMachines();
    this.error = null;
  }

  getMachines(): void {
    this.machineService.getMachines().subscribe(mdf => this.machines = mdf);
  }

  addMachine(position: number, machineTypeId: number): void {
    if (!position) {
      this.error = "Position cannot be empty";
      return;
    }
    if (!machineTypeId) {
      this.error = "Id of a machine type cannot be empty";
      return;
    }

    this.machineService.addMachine({ position, machineTypeId } as Machine)
      .subscribe(machine => {
        if (machine) {
          this.machines.push(machine);
          this.error = null;
        } else {
          this.error = "Failed to add a new machine.";
        }
      });
  }
}
