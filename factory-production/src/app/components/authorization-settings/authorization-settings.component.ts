import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CustomerService } from 'src/app/services/customer/customer.service'

@Component({
  selector: 'app-authorization-settings',
  templateUrl: './authorization-settings.component.html',
  styleUrls: ['./authorization-settings.component.css']
})
export class AuthorizationSettingsComponent implements OnInit {
  settings: any;
  updateOrder: false;
  insertOrder: false;
  consultOrder: false;
  consultAllOrders: false;
  updateCustomer: false;
  consultCustomer: false;
  consultAllCustomers: false;
  consultAllCustomerOrders: false;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private customerService: CustomerService
  ) { }

  ngOnInit() {
    this.getSettings();
  }

  getSettings(): void {
    this.customerService.getAuthorizationSettings()
      .subscribe(settings => {
        this.settings = settings;
        this.updateOrder = settings.updateOrder.customer;
        this.insertOrder = settings.insertOrder.customer;
        this.consultOrder = settings.consultOrder.customer;
        this.consultAllOrders = settings.consultAllOrders.customer;
        this.updateCustomer = settings.updateCustomer.customer;
        this.consultCustomer = settings.consultCustomer.customer;
        this.consultAllCustomers = settings.consultAllCustomers.customer;
        this.consultAllCustomerOrders = settings.consultAllCustomerOrders.customer;
      });
  }

  setUpdateOrder() {
    const json = {
      "category": "updateOrder",
      "value": this.toggle(this.updateOrder)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  setInsertOrder() {
    const json = {
      "category": "insertOrder",
      "value": this.toggle(this.insertOrder)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  setConsultOrder() {
    const json = {
      "category": "consultOrder",
      "value": this.toggle(this.consultOrder)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  setConsultAllOrders() {
    const json = {
      "category": "consultAllOrders",
      "value": this.toggle(this.consultAllOrders)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  setUpdateCustomer() {
    const json = {
      "category": "updateCustomer",
      "value": this.toggle(this.updateCustomer)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  setConsultCustomer() {
    const json = {
      "category": "consultCustomer",
      "value": this.toggle(this.consultCustomer)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  setConsultAllCustomers() {
    const json = {
      "category": "consultAllCustomers",
      "value": this.toggle(this.consultAllCustomers)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  setConsultAllCustomerOrders() {
    const json = {
      "category": "consultAllCustomerOrders",
      "value": this.toggle(this.consultAllCustomerOrders)
    }
    this.customerService.updateAuthorizationSettings(json).subscribe(settings => { });
  }

  toggle(currentValue) {
    if (currentValue) {
      return false;
    } else {
      return true;
    }
  }

  goBack(): void {
    this.location.back();
  }

}
