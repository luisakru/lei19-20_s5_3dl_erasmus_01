import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { CustomerSignIn } from "src/app/types/CustomerSignIn";
import { CustomerService } from "src/app/services/customer/customer.service";

@Component({
  selector: "signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.css"]
})
export class SigninComponent implements OnInit {
  name: String;
  password: String;
  error: string;

  @Output() login = new EventEmitter<{
    isLoggedIn: boolean;
    isAdmin: boolean;
  }>();

  constructor(private customerService: CustomerService) {}

  ngOnInit() {
    this.error = null;
  }

  signIn(name: string, password: string): void {
    if (!name) {
      this.error = "Name cannot be empty";
      return;
    }
    if (!password) {
      this.error = "Password cannot be empty";
      return;
    }

    this.customerService
      .signIn({ name, password } as CustomerSignIn)
      .subscribe(response => {
        if (response && response.user && response.token) {
          localStorage.setItem("token", response.token);
          localStorage.setItem("id", response.user._id);
          localStorage.setItem("isCustomer", response.user.type_customer);

          this.login.emit({
            isLoggedIn: true,
            isAdmin: !response.user.type_customer
          });
        } else {
          this.error = "Wrong name or password";
        }
      });
  }
}
