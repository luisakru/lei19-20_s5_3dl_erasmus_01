import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerOrdersDetailComponent } from './customer-orders-detail.component';

describe('CustomerOrdersDetailComponent', () => {
  let component: CustomerOrdersDetailComponent;
  let fixture: ComponentFixture<CustomerOrdersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerOrdersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerOrdersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
