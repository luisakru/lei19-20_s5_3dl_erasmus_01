import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/types/order';
import { Customer } from 'src/app/types/customer';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { OrderService } from 'src/app/services/order/order.service';
import { Location } from '@angular/common';
import { Product } from 'src/app/types/product';
import { MdpService } from 'src/app/services/mdp/mdp.service';

@Component({
  selector: 'app-customer-orders-detail',
  templateUrl: './customer-orders-detail.component.html',
  styleUrls: ['./customer-orders-detail.component.css']
})
export class CustomerOrdersDetailComponent implements OnInit {

  orders: Order[];
  customer: Customer;
  products: Product[];

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private orderService: OrderService,
    private location: Location,
    private mdpService: MdpService
  ) {}

  ngOnInit(): void {
    this.getCustomer();
    this.getOrderById();
    this.getOrders();
    this.getProducts();
  }

  getProducts(): void {
    this.mdpService.getProducts()
      .subscribe(products => this.products = products);
  }
  
  getCustomer(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.customerService.getCustomer(id)
      .subscribe(customer => this.customer = customer) 
  }
  
  getOrderById(): void {
    this.orderService.getOrders()
      .subscribe(orders => this.orders = orders);
  }

  getOrders(): void {
    this.orderService.getOrders()
      .subscribe(orders => this.orders = orders);
  }

  // updateOrder(): void {
  //   this.orderService.updateOrder()
  //     .subscribe(orders => this.orders = orders);
  // }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.customerService.updateCustomer(this.customer)
      .subscribe(() => this.goBack());
  }

}
