import { Component, OnInit, Input } from '@angular/core';
import { Customer } from 'src/app/types/customer';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Location } from '@angular/common';

@Component({
  selector: 'customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {
  [x: string]: any;
  @Input() customer: Customer;

  isEditing: boolean;
  editCustomer: Customer;

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private location: Location
  ) { }

  ngOnInit(): void {
      this.getCustomer();
  }

  getCustomer(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.customerService.getCustomer(id)
      .subscribe(customer => {
        this.customer = customer
      }) 
  }

  edit(): void {
    this.getCustomer();
    this.editCustomer =  Object.assign({}, this.customer);
    this.isEditing = true;
  }

  save(): void {
    this.customerService.updateCustomerAdmin(this.editCustomer).subscribe(res => {
      this.getCustomer();
      this.isEditing = false;
    });
  }

  goBack(): void {
    this.location.back();
  }
}
