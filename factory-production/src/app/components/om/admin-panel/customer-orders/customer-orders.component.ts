import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Customer } from 'src/app/types/customer';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Order } from 'src/app/types/order';

@Component({
  selector: 'customer-orders',
  templateUrl: './customer-orders.component.html',
  styleUrls: ['./customer-orders.component.css']
})
export class CustomerOrdersComponent implements OnInit {
  [x: string]: any;
  @Input() customer: Customer;

  orders: Order[];

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getAllOrders();
    this.getCustomer();
  }

  getAllOrders (): void {
    this.customerService.getAllOrders()
      .subscribe(orders => {
        this.orders = orders;
      }) 
  }

  getCustomer(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.customerService.getCustomer(id)
      .subscribe(customer => {
        this.customer = customer
      }) 
  }

  goBack(): void {
    this.location.back();
  }

  stringAsDate(dateStr: string) {
    return new Date(dateStr);
  }

  hasProp(o, name) {
    return o.hasOwnProperty(name);
  }

}
