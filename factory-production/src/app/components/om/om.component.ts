import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth.service";

@Component({
  selector: "om",
  templateUrl: "./om.component.html",
  styleUrls: ["./om.component.css"]
})
export class OmComponent implements OnInit  {
  isLoggedIn: boolean;
  isAdmin: boolean;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.isLoggedIn =
      !!this.authService.getToken() && !!this.authService.getUsersId();
    this.isAdmin = this.authService.isAdmin();
    console.log('isAdmin: ' + this.isAdmin);
  }
  
}
