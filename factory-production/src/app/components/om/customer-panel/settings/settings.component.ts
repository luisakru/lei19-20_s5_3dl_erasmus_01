import { Component, OnInit } from "@angular/core";
import { CustomerService } from "src/app/services/customer/customer.service";
import { Customer } from "src/app/types/customer";

@Component({
  selector: "settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.css"]
})
export class SettingsComponent implements OnInit {
  constructor(private customerService: CustomerService) {}

  customer: Customer;
  error: String;

  isEditing: boolean;
  editCustomer: Customer;

  ngOnInit() {
    this.error = null;
    this.isEditing = false;
    this.getCurrentCustomer();
  }

  getCurrentCustomer(): void {
    this.customerService.getCurrentCustomer().subscribe(customer => {
      if (customer) {
        this.customer = customer as Customer;
        this.error = null;
      }
      else this.error = "Cannot load customer data";
    });
  }

  edit(): void {
    this.getCurrentCustomer()
    this.editCustomer =  Object.assign({}, this.customer);
    this.isEditing = true;
  }

  save(): void {
    this.customerService.updateCustomer(this.editCustomer).subscribe(res => {
      this.getCurrentCustomer()
      this.isEditing = false;
    });
  }
}


