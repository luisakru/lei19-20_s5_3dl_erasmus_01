import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/types/order';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Location } from "@angular/common";

@Component({
  selector: 'app-my-order-edit',
  templateUrl: './my-order-edit.component.html',
  styleUrls: ['./my-order-edit.component.css']
})
export class MyOrderEditComponent implements OnInit {
  [x: string]: any;
  @Input() order: Order;

  isEditing: boolean;
  editOrder: Order;

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getOrder();
  }

  getOrder(): void {
    const customerService = this.customerService;

    const id = this.route.snapshot.paramMap.get("id");
    this.customerService.getOrder(id).subscribe(order => {
      this.order = order;
    });
  }

  edit(): void {
    this.editOrder = Object.assign({}, this.order);
    this.isEditing = true;
  }

  save(): void {
    this.customerService.updateOrder(this.editOrder).subscribe(res => {
      this.getOrder();
      this.isEditing = false;
    });
  }

  cancel(): void {
    this.customerService.cancelOrder(this.order).subscribe(res => {
      this.getOrder();
    });
  }

  goBack(): void {
    this.location.back();
  }

  stringAsDate(dateStr: string) {
    return new Date(dateStr);
  }

  hasProp(o, name) {
    return o.hasOwnProperty(name);
  }

}
