import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyOrderEditComponent } from './my-order-edit.component';

describe('MyOrderEditComponent', () => {
  let component: MyOrderEditComponent;
  let fixture: ComponentFixture<MyOrderEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyOrderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyOrderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
