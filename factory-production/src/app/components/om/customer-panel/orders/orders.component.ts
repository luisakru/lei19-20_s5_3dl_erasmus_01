import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order/order.service';
import { Order } from 'src/app/types/order';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {

  selectedOrder: Order;
  orders: Order[];

  constructor(private customerService: CustomerService) { }

  ngOnInit() {
    this.getOrders();
  }

  onSelect(order: Order): void {
    this.selectedOrder = order;
  }

  getOrders(): void {
    this.customerService.getCustomerOrders()
      .subscribe(orders => {
        this.orders = orders
      });
  }

  stringAsDate(dateStr: string) {
    return new Date(dateStr);
  }

  hasProp(o, name) {
    return o.hasOwnProperty(name);
  }

}
