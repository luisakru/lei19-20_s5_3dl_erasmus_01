import { Component, OnInit } from "@angular/core";
import { MdpComponent } from "src/app/components/mdp/mdp.component";
import { Product } from "src/app/types/product";
import { MdpService } from "src/app/services/mdp/mdp.service";
import { CustomerService } from 'src/app/services/customer/customer.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: "shop",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.css"]
})
export class ShopComponent implements OnInit {
  products: Product[];
  selectedProductId: number;
  error: string;
  orderMade: string;

  constructor(
    public mdpService: MdpService,
    public customerService: CustomerService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getProducts();
    this.error = null;
    this.orderMade = null;
  }

  getProducts(): void {
    this.mdpService.getProducts().subscribe(products => {
      this.products = products;
      this.selectedProductId = this.products[0].id;
    });
  }

  order(quantity: number): void {
    this.customerService.createOrder(this.selectedProductId, quantity).subscribe(res => {
      if(res && res.status) {
        this.error = null;
        this._snackBar.open("The order has been made", "Okay", {
          duration: 1500,
          panelClass: ['snackbar']
        });
      } else {
        this.error = "Cannot make an order"
      }
    })
  }

  setProduct(e: any): void {
    this.selectedProductId = e;
  }
}
