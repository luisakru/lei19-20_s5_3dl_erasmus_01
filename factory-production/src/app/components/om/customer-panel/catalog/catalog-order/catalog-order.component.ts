import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/types/product';
import { ManufactoringPlan } from 'src/app/types/manufactoringPlan';
import { ActivatedRoute } from '@angular/router';
import { MdpService } from 'src/app/services/mdp/mdp.service';
import { Location } from '@angular/common';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-catalog-order',
  templateUrl: './catalog-order.component.html',
  styleUrls: ['./catalog-order.component.css']
})
export class CatalogOrderComponent implements OnInit {
  product: Product;
  mp: ManufactoringPlan;
  error: string;

  constructor(
    private route: ActivatedRoute,
    private mdpService: MdpService,
    private location: Location,
    public customerService: CustomerService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getProduct();
    this.getManufactoringPlan();
  }

  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.mdpService.getProduct(id)
      .subscribe(product => {
        this.product = product;
      });
  }


  getManufactoringPlan(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.mdpService.getManufactoringPlan(id)
      .subscribe(mp => {
        this.mp = mp;
      });
  }

  order(quantity: number): void {
    this.customerService.createOrder(this.product.id, quantity).subscribe(res => {
      if (res && res.status) {
        this.error = null;
        this._snackBar.open("The order has been made", "Okay", {
          duration: 1500,
          panelClass: ['snackbar']
        });
      } else {
        this.error = "Cannot make an order"
      }
    })
  }

  goBack(): void {
    this.location.back();
  }

}
