import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/types/product';
import { Order } from 'src/app/types/order';
import { MdpService } from 'src/app/services/mdp/mdp.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  products: Product[];
  orders: Order[];
  sortedData: Product[];

  constructor(
    public mdpService: MdpService,
    private customerService: CustomerService,
  ) { }

  ngOnInit() {
    this.getProducts();
    this.getAllOrders();
  }


  getProducts(): void {
    this.mdpService.getProducts()
      .subscribe(products => this.products = products);
  }

  addProduct(manufactoringPlanId: number): void {
    if (!manufactoringPlanId) { return; }
    this.mdpService.addProduct({ manufactoringPlanId } as Product)
      .subscribe(product => {
        if (product != undefined) {
          this.products.push(product);
        }
      });
  }

  getAllOrders(): void {
    this.customerService.getAllOrders()
      .subscribe(orders => {
        this.orders = orders;
      })
  }

  calculateNumberOfOrdersOfProduct(id: number): number {
    console.log("inside calculateNumberOfOrdersOfProduct")
    let counter = 0;
    this.orders.forEach(order => {
      if (order.product == id) {
        console.log(order.status);
        if (order.status != "canceld") {
          counter++;
        }
      }
    });
    return counter;
  }

  calculatQuantityOfProduct(id: number): number {
    let counter = 0;
    this.orders.forEach(order => {
      if (order.product == id) {
        if (order.status != "canceld") {
          counter += order.quantity;
        }
      }
    });
    return counter;
  }

  sortData(sort: Sort) {
    const data = this.products.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id': return compare(a.id, b.id, isAsc);
        case 'orders': return compare(this.calculateNumberOfOrdersOfProduct(a.id), this.calculateNumberOfOrdersOfProduct(b.id), isAsc);
        case 'quantity': return compare(this.calculatQuantityOfProduct(a.id), this.calculatQuantityOfProduct(b.id), isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}