import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/types/customer';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'customer-panel',
  templateUrl: './customer-panel.component.html',
  styleUrls: ['./customer-panel.component.css']
})
export class CustomerPanelComponent implements OnInit {

  // selectedCustomer: Customer;

  // customers: Customer[];

  // constructor(private customersService: CustomerService) { }

  ngOnInit() {
    // this.getCustomers();
  }

  // onSelect(customer: Customer): void {
  //   this.selectedCustomer = customer;
  // }

  // getCustomers(): void {
  //   this.customersService.getCustomers()
  //     .subscribe(customers => this.customers = customers);
  // }
}
