import { Component, OnInit } from "@angular/core";
import { CustomerService } from "src/app/services/customer/customer.service";
import { CustomerSignUp } from "src/app/types/CustomerSignUp";

@Component({
  selector: "signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  name: String;
  address: String;
  password: String;
  error: string;
  registered: boolean;

  constructor(private customerService: CustomerService) {}

  ngOnInit() {
    this.error = null;
    this.registered = false;
  }

  signUp(name: string, address: string, password: string): void {
    if (!name) {
      this.error = "Name cannot be empty";
      return;
    }
    if (!password) {
      this.error = "Password cannot be empty";
      return;
    }
    if (!address) {
      this.error = "Address cannot be empty";
      return;
    }

    this.customerService
      .signUp({ name, address, password } as CustomerSignUp)
      .subscribe(user => {
        if (user) {
          this.registered = true;
        } else {
          this.error = "Cannot create a new account";
        }
      });
  }
}
