import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Product } from '../../../types/product';
import { ManufactoringPlan } from '../../../types/manufactoringPlan';
import { MdpService } from '../../../services/mdp/mdp.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product: Product;
  mp: ManufactoringPlan;

  constructor(
    private route: ActivatedRoute,
    private mdpService: MdpService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getProduct();
    this.getManufactoringPlan();
  }

  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.mdpService.getProduct(id)
      .subscribe(product => {
        this.product = product;
      });
  }

  getManufactoringPlan(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.mdpService.getManufactoringPlan(id)
      .subscribe(mp => {
        this.mp = mp;
      });
  }

  goBack(): void {
    this.location.back();
  }

}
