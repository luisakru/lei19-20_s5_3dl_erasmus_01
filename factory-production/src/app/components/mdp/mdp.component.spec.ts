import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MdpComponent } from './mdp.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { By } from '@angular/platform-browser';

describe('MdpComponent', () => {
  let component: MdpComponent;
  let fixture: ComponentFixture<MdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, FormsModule, HttpClientModule ],
      declarations: [ MdpComponent  ], 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getProducts() of mdpService on component Init', () => {
    spyOn(component.mdpService, 'getProducts').and.callThrough();
    component.ngOnInit();
    expect(component.mdpService.getProducts).toHaveBeenCalled();
  });

  it('should contain header "Products!"', () => {
    const ele: HTMLElement = fixture.nativeElement;
    expect(ele.textContent).toContain('Products');
  });


  // it('should have <h2> with "Products"', () => {
  //   const bannerElement: HTMLElement = fixture.nativeElement;
  //   fixture.detectChanges();
  //   const h2 = bannerElement.querySelector('h2');
  //   console.log(h2.textContent);
  //   expect(h2.textContent).toEqual('Products');
  // });


  // it('should render Product List in HTML', () => {
  //   const ele = fixture.debugElement.query(By.css('.products'));
  //   console.log(ele.children.length);
  //  // expect(ele.childNodes[1].innerHTML).not.toBeNull();
  //  // expect(ele.childNodes[2].innerHTML).not.toBeNull();
  // });

});
