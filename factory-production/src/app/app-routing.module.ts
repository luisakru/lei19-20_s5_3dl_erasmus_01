import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ProductDetailComponent } from "./components/mdp/product-detail/product-detail.component";
import { MdfComponent } from "./components/mdf/mdf.component";
import { MdpComponent } from "./components/mdp/mdp.component";
import { OperationDetailComponent } from "./components/mdf/operation-detail/operation-detail.component";
import { MachineDetailComponent } from "./components/mdf/machine-detail/machine-detail.component";
import { MachinetypeDetailComponent } from "./components/mdf/machinetype-detail/machinetype-detail.component";
import { ProductionlineDetailComponent } from "./components/mdf/productionline-detail/productionline-detail.component";
import { OmComponent } from "./components/om/om.component";
import { OrdersComponent } from "./components/om/customer-panel/orders/orders.component";
import { SettingsComponent } from "./components/om/customer-panel/settings/settings.component";
import { ShopComponent } from "./components/om/customer-panel/shop/shop.component";
import { CustomerEditComponent } from "./components/om/admin-panel/customer-edit/customer-edit.component";
import { CustomerOrdersComponent } from "./components/om/admin-panel/customer-orders/customer-orders.component";
import { SignupComponent } from "./components/signup/signup.component";
import { OrderEditComponent } from "./components/om/admin-panel/order-edit/order-edit.component";
import { CustomersComponent } from './components/om/admin-panel/customers/customers.component';
import { MyOrderEditComponent } from './components/om/customer-panel/orders/my-order-edit/my-order-edit.component';
import { AuthorizationSettingsComponent } from './components/authorization-settings/authorization-settings.component';
import { CatalogComponent } from './components/om/customer-panel/catalog/catalog.component';
import { CatalogOrderComponent } from './components/om/customer-panel/catalog/catalog-order/catalog-order.component';

const routes: Routes = [
  { path: "mdf", component: MdfComponent },
  { path: "mdp", component: MdpComponent },
  { path: "om", component: OmComponent },
  { path: "authorization-settings", component: AuthorizationSettingsComponent },
  { path: "mdp/product/detail/:id", component: ProductDetailComponent },
  { path: "mdf/operation-detail/:id", component: OperationDetailComponent },
  { path: "mdf/machine-detail/:id", component: MachineDetailComponent },
  { path: "mdf/machinetype-detail/:id", component: MachinetypeDetailComponent },
  {
    path: "mdf/productionline-detail/:id",
    component: ProductionlineDetailComponent
  },
  { path: "om/shop", component: ShopComponent },
  { path: "om/orders", component: OrdersComponent },
  { path: "om/settings", component: SettingsComponent },
  { path: "om/catalog", component: CatalogComponent },
  { path: "om/catalog/order/:id", component: CatalogOrderComponent },

  { path: "signup", component: SignupComponent },

  { path: "om/all-orders", component: CustomerOrdersComponent },
  { path: "om/customers", component: CustomersComponent },

  { path: "om/customer-edit/:id", component: CustomerEditComponent },
  { path: "om/all-orders/order-edit/:id", component: OrderEditComponent },
  { path: "om/all-my-orders/order-edit/:id", component: MyOrderEditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
