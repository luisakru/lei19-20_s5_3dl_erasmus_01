import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Factory Production';

  isLoggedIn: boolean;
  isAdmin: boolean;
  isSignUp: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.isLoggedIn =
      !!this.authService.getToken() && !!this.authService.getUsersId();
    this.isAdmin = this.authService.isAdmin();
    this.isSignUp = false;
  }

  onLogin(response: { isLoggedIn: boolean; isAdmin: boolean }) {
    this.isLoggedIn = response.isLoggedIn;
    this.isAdmin = response.isAdmin;
  }

  goToSignUp() {
    this.isSignUp = true;
  }

  goToSignIn() {
    this.isSignUp = false;
  }
}
