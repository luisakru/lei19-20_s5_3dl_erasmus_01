import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatSortModule } from '@angular/material/sort';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { ProductDetailComponent } from './components/mdp/product-detail/product-detail.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MdfComponent } from './components/mdf/mdf.component';
import { MdpComponent } from './components/mdp/mdp.component';
import { OperationDetailComponent } from './components/mdf/operation-detail/operation-detail.component';
import { MessagesComponent } from './components/messages/messages.component';
import { MachineDetailComponent } from './components/mdf/machine-detail/machine-detail.component';
import { MachinetypeDetailComponent } from './components/mdf/machinetype-detail/machinetype-detail.component';
import { OperationsComponent } from './components/mdf/operations/operations.component';
import { MachinetypesComponent } from './components/mdf/machinetypes/machinetypes.component';
import { MachinesComponent } from './components/mdf/machines/machines.component';
import { ProductionlineComponent } from './components/mdf/productionlines/productionlines.component';
import { ProductionlineDetailComponent } from './components/mdf/productionline-detail/productionline-detail.component';
import { OmComponent } from './components/om/om.component';
import { CustomerPanelComponent } from './components/om/customer-panel/customer-panel.component';
import { AdminPanelComponent } from './components/om/admin-panel/admin-panel.component';
import { ShopComponent } from './components/om/customer-panel/shop/shop.component';
import { OrdersComponent } from './components/om/customer-panel/orders/orders.component';
import { SettingsComponent } from './components/om/customer-panel/settings/settings.component';
import { CustomerOrdersComponent } from './components/om/admin-panel/customer-orders/customer-orders.component';
import { CustomerEditComponent } from './components/om/admin-panel/customer-edit/customer-edit.component';
import { CustomerOrdersDetailComponent } from './components/om/admin-panel/customer-orders-detail/customer-orders-detail.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { OrderEditComponent } from './components/om/admin-panel/order-edit/order-edit.component';
import { CustomersComponent } from './components/om/admin-panel/customers/customers.component';
import { MyOrderEditComponent } from './components/om/customer-panel/orders/my-order-edit/my-order-edit.component';
import { AuthorizationSettingsComponent } from './components/authorization-settings/authorization-settings.component';
import { CatalogComponent } from './components/om/customer-panel/catalog/catalog.component';
import { CatalogOrderComponent } from './components/om/customer-panel/catalog/catalog-order/catalog-order.component';



@NgModule({
  declarations: [
    AppComponent,
    MdfComponent,
    MdpComponent,
    ProductDetailComponent,
    OperationDetailComponent,
    MessagesComponent,
    MachineDetailComponent,
    MachinetypeDetailComponent,
    OperationsComponent,
    MachinetypesComponent,
    MachinesComponent,
    ProductionlineComponent,
    ProductionlineDetailComponent,
    OmComponent,
    CustomerPanelComponent,
    AdminPanelComponent,
    ShopComponent,
    OrdersComponent,
    SettingsComponent,
    CustomerOrdersComponent,
    CustomerEditComponent,
    CustomerOrdersDetailComponent,
    CustomerEditComponent,
    SigninComponent,
    SignupComponent,
    OrderEditComponent,
    CustomersComponent,
    MyOrderEditComponent,
    AuthorizationSettingsComponent,
    CatalogComponent,
    CatalogOrderComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatSnackBarModule
  ],
  providers: [
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
