import { Customer } from './types/customer';

export const CUSTOMERS: Customer[] = [

  { id: '0', name: 'Zosia', address: 'ISEP'},
  { id: '2', name: 'Luisa', address: 'ISEP'},
  { id: '3', name: 'Nuno', address: 'ISEP'},
  { id: '4', name: 'Ricardo', address: 'ISEP'},
  { id: '5', name: 'Phil', address: 'ISEP'},
  { id: '6', name: 'Mai', address: 'ISEP'},
  { id: '7', name: 'April', address: 'ISEP'},
  { id: '8', name: 'Juli', address: 'ISEP'},
  { id: '9', name: 'Juni', address: 'ISEP'},
];