import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MessageService } from '../message/message.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Order } from 'src/app/types/order';
import { ORDERS } from 'src/app/mock-orders';
import { AuthService } from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private orderUrl = "http://localhost:3000/order";

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getHttpOptions(): { headers: HttpHeaders } {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `${this.authService.getToken()}`
      })
    };
    return httpOptions;
  }

  constructor(private messageService: MessageService,
    private authService: AuthService) { }

  getOrders(): Observable<Order[]> {
    this.messageService.add('OrderService: fetched orders');
    return of(ORDERS);
  }


  // getOrders(): Observable<Order[]> {

  //   return this.http.get<Order[]>(this.ordersUrl)
  //   // .pipe(
  //   //     tap(_ => this.log('fetched orders')),
  //   //     catchError(this.handleError<Order[]>('getOrders', []))
  //   //   );
  // }

  // getOrder(id: number): Observable<Order> {
  //   const url = `${this.ordersUrl}/${id}`;
  //   return this.http.get<Order>(url)
  //   // .pipe(
  //   // tap(_ => this.log(`fetched order id=${id}`)),
  //   // catchError(this.handleError<OrdersComponent>(`getOrder id=${id}`))
  //   // );
  // }

  // addOrder(orders: Order): Observable<Order> {
  //   const postRequest = this.http.post<Order>(this.ordersUrl, orders, this.httpOptions);
  //   return postRequest.pipe(
  //     catchError(this.handleError<Order>('addOrders'))
  //   );
  // }

  // private handleError<T>(operationFail = 'operationFail', result?: T) {
  //   return (error: any): Observable<T> => {
  //     console.error(error);
  //     return of(result as T);
  //   };
  // }

}
