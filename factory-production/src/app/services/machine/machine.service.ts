import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Machine } from '../../types/machine';
import { MessageService } from '../message/message.service';
import { MachineType } from 'src/app/types/machinetype';



@Injectable({ providedIn: 'root' })
export class MachineService {

  // private machineUrl = 'https://mdferasmus.azurewebsites.net/api/machine';  // URL to web api
  private machineUrl = 'https://localhost:5001/api/machine';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }


  /** GET machines from the server */
  getMachines(): Observable<Machine[]> {
    return this.http.get<Machine[]>(this.machineUrl)
      .pipe(
        tap(_ => this.log('fetched machines')),
        catchError(this.handleError<Machine[]>('getMachines', []))
      );
  }

  /** GET machines by id. Return `undefined` when id not found */
  getMachineNo404<Data>(id: number): Observable<Machine> {
    const url = `${this.machineUrl}/?id=${id}`;
    return this.http.get<Machine[]>(url)
      .pipe(
        map(mdf => mdf[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} machine id=${id}`);
        }),
        catchError(this.handleError<Machine>(`getMachine id=${id}`))
      );
  }

  /** GET machine by id. Will 404 if id not found */
  getMachine(id: number): Observable<Machine> {
    const url = `${this.machineUrl}/${id}`;
    return this.http.get<Machine>(url)
    // .pipe(
    //   tap(_ => this.log(`fetched machine id=${id}`)),
    //   catchError(this.handleError<Machine>(`getMachine id=${id}`))
    // );
  }

  /** POST: add a new machine to the server */
  addMachine(machine: Machine): Observable<Machine> {
    const postRequest = this.http.post<Machine>(this.machineUrl, machine, this.httpOptions);
    return postRequest.pipe(
      tap((newMachine: Machine) => this.log(`added machine w/ id=${newMachine.id}`)),
      catchError(this.handleError<Machine>('addMachine'))
    );
  }

  /** DELETE: delete the machine from the server */
  deleteMachine(machine: Machine | number): Observable<Machine> {
    const id = typeof machine === 'number' ? machine : machine.id;
    const url = `${this.machineUrl}/${id}`;

    return this.http.delete<Machine>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted machine id=${id}`)),
      catchError(this.handleError<Machine>('deleteMachine'))
    );
  }

  /** PUT: update the machine on the server */
  updateMachine(machine: Machine): Observable<any> {
    const url = `${this.machineUrl}/${machine.id}`;
    return this.http.put(url, machine, this.httpOptions).pipe(
      tap(_ => this.log(`updated machine id=${machine.id}`)),
      catchError(this.handleError<any>('updateMachine'))
    );
  }

  updateMachineTypeofMachine(machine: Machine): Observable<any> {
    const idInt = Number(machine.machineTypeId)
    const url = `${this.machineUrl}/${machine.id}`;
    machine.machineTypeId = idInt
    return this.http.put(url, machine, this.httpOptions).pipe(
      tap(_ => this.log(`updated machinetype id=${machine.id}`)),
      catchError(this.handleError<any>('updateMachineTypeofMachines'))
    )
  }

  /**
   * Handle Http operationFail that failed.
   * Let the app continue.
   * @param operationFail - name of the operationFail that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operationFail = 'operationFail', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operationFail} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a MachineService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MachineService: ${message}`);
  }

}
