/* 
- HttpClientTestingModule: to mock HttpClientModule, because we don’t want to make actual 
http requests while testing the service.
- HttpTestingController is injected into tests, that allows for mocking and flushing of requests.
- httpMock.verify() is called after each tests to verify that there are no outstanding http calls
*/

import { TestBed, getTestBed } from '@angular/core/testing';

import { MdpService } from './mdp.service';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Product } from 'src/app/types/product';

describe('MdpService', () => {
  let injector: TestBed;
  let service: MdpService;
  let httpMock: HttpTestingController;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule],
    });
    injector = getTestBed();
    service = injector.get(MdpService);
    httpMock = injector.get(HttpTestingController);;
  });


  it('should be created', () => {
    const service: MdpService = TestBed.get(MdpService);
    expect(service).toBeTruthy();
  });


  const exptectedGetProducts = [
      { "id": 1, "manufactoringPlanId": 1 },
      { "id": 2, "manufactoringPlanId": 2 }
  ]; 
  it('getProducts() should return data', () => {
      service.getProducts().subscribe((res) => {
        expect(res).toEqual(exptectedGetProducts);
      });
  
      const req = httpMock.expectOne('https://localhost:5003/api/product');
      expect(req.request.method).toBe('GET');
      req.flush(exptectedGetProducts);
  });


  const exptectedGetProductById = { "id": 1, "manufactoringPlanId": 1 }; 
  it ('getProduct() should return data', () => {
    service.getProduct(1).subscribe((res) => {
      expect(res).toEqual(exptectedGetProductById);
    });

    const req = httpMock.expectOne('https://localhost:5003/api/product/1');
    expect(req.request.method).toBe('GET');
    req.flush(exptectedGetProductById); 
  });


  it('addProduct() should post the correct data', () => {
    let newProduct = new Product();
    newProduct.manufactoringPlanId = 2;

    service.addProduct(newProduct).subscribe((data: Product) => {
        expect(data.manufactoringPlanId).toBe(2);
      });
  
    const req = httpMock.expectOne(
      'https://localhost:5003/api/product',
      'post to api'
    );
    expect(req.request.method).toBe('POST');
  
    req.flush({
      manufactoringPlanId : 2
    });
  
    httpMock.verify();
  });
  
});
