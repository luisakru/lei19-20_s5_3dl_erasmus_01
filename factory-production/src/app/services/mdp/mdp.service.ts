import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Product } from '../../types/product';
import { ManufactoringPlan } from '../../types/manufactoringPlan';
import { MessageService } from '../message/message.service';

@Injectable({
  providedIn: 'root'
})
export class MdpService {
  private productsUrl = 'https://localhost:5003/api/product';

  httpOptions = {
    headers: new HttpHeaders({
     // 'Content-Type': 'application/json',
      //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  /** GET product from the server */
  getProducts (): Observable<Product[]> {
    return this.http.get<Product[]>(this.productsUrl, this.httpOptions) 
      .pipe(
        tap(_ => this.log('fetched Productes')),
        catchError(this.handleError<Product[]>('getProductes', []))
      );
  }

  /** GET product by id. Return `undefined` when id not found */
  getProductNo404<Data>(id: number): Observable<Product> {
    const url = `${this.productsUrl}/?id=${id}`;
    return this.http.get<Product[]>(url, this.httpOptions)
      .pipe(
        map(products => products[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} product id=${id}`);
        }),
        catchError(this.handleError<Product>(`getProduct id=${id}`))
      );
  }

  /** GET product by id. Will 404 if id not found */
  getProduct(id: number): Observable<Product> {
    const url = `${this.productsUrl}/${id}`;
    return this.http.get<Product>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched product id=${id}`)),
      catchError(this.handleError<Product>(`getProduct id=${id}`))
    );
  }

  getManufactoringPlan(id: number): Observable<ManufactoringPlan> {
    const url = `${this.productsUrl}/${id}/manufactoringplan`;
    return this.http.get<ManufactoringPlan>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched manufactoringPlan id=${id}`)),
      catchError(this.handleError<ManufactoringPlan>(`getManufactoringPlan id=${id}`))
    );
  }

  //////// Save methods //////////

  /** POST: add a new product to the server */
  addProduct(product: Product): Observable<Product> {
    const http$ = this.http.post<Product>(this.productsUrl, product, this.httpOptions); 

    return http$.pipe(
      tap((newProduct: Product) => this.log(`added product w/ id=${newProduct.id}`)),
      catchError(this.handleError<Product>('addProduct'))
    );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a ProductService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`ProductService: ${message}`);
  }
}
