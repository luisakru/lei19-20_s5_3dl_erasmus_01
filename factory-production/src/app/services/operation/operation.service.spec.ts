import { TestBed } from '@angular/core/testing';

import { OperationService } from './operation.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

describe('OperationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  imports: [RouterTestingModule, HttpClientModule],
}));

  it('should be created', () => {
    const service: OperationService = TestBed.get(OperationService);
    expect(service).toBeTruthy();
  });
});
