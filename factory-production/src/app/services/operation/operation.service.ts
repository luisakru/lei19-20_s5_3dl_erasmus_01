import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Operation } from '../../types/operation';
import { MessageService } from '../message/message.service';


@Injectable({ providedIn: 'root' })
export class OperationService {

  // private operationUrl = 'https://mdferasmus.azurewebsites.net/api/operation';  // URL to web api
  private operationUrl = 'https://localhost:5001/api/operation';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }


  /** GET operations from the server */
  getOperations(): Observable<Operation[]> {
    return this.http.get<Operation[]>(this.operationUrl)
      .pipe(
        tap(_ => this.log('fetched operations')),
        catchError(this.handleError<Operation[]>('getOperations', []))
      );
  }

  /** GET operation by id. Return `undefined` when id not found */
  getOperationNo404<Data>(id: number): Observable<Operation> {
    const url = `${this.operationUrl}/?id=${id}`;
    return this.http.get<Operation[]>(url)
      .pipe(
        map(mdf => mdf[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} operation id=${id}`);
        }),
        catchError(this.handleError<Operation>(`getOperation id=${id}`))
      );
  }

  /** GET operation by id. Will 404 if id not found */
  getOperation(id: number): Observable<Operation> {
    const url = `${this.operationUrl}/${id}`;
    return this.http.get<Operation>(url).pipe(
      tap(_ => this.log(`fetched operation id=${id}`)),
      catchError(this.handleError<Operation>(`getOperation id=${id}`))
    );
  }


  /* GET operations whose name contains search term */
  // searchOperation(term: string): Observable<Operation[]> {
  //   if (!term.trim()) {
  //     // if not search term, return empty operation array.
  //     return of([]);
  //   }
  //   return this.http.get<Operation[]>(`${this.operationUrl}/?name=${term}`).pipe(
  //     tap(_ => this.log(`found operations matching "${term}"`)),
  //     catchError(this.handleError<Operation[]>('searchOperation', []))
  //   );
  // }

  //////// Save methods //////////

  /** POST: add a new operation to the server */
  addOperation(operation: Operation): Observable<Operation> {
    return this.http.post<Operation>(this.operationUrl, operation, this.httpOptions).pipe(
      tap((newOperation: Operation) => this.log(`added operation w/ id=${newOperation.id}`)),
      catchError(this.handleError<Operation>('addOperation'))
    );
  }

  /** DELETE: delete the operation from the server */
  deleteOperation(operation: Operation | number): Observable<Operation> {
    const id = typeof operation === 'number' ? operation : operation.id;
    const url = `${this.operationUrl}/${id}`;

    return this.http.delete<Operation>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted operation id=${id}`)),
      catchError(this.handleError<Operation>('deleteOperation'))
    );
  }

  /** PUT: update the operation on the server */
  updateOperation(operation: Operation): Observable<any> {
    return this.http.put(this.operationUrl, operation, this.httpOptions).pipe(
      tap(_ => this.log(`updated operation id=${operation.id}`)),
      catchError(this.handleError<any>('updateOperation'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operationFail - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operationFail = 'operationFail', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operationFail} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a OperationService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`OperationService: ${message}`);
  }

}
