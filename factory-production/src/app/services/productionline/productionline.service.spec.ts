import { TestBed } from '@angular/core/testing';

import { ProductionlineService } from './productionline.service';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http'; 

describe('ProductionlineService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule, HttpClientModule],
  }));

  it('should be created', () => {
    const service: ProductionlineService = TestBed.get(ProductionlineService);
    expect(service).toBeTruthy();
  });
});
