import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MessageService } from '../message/message.service';
import { Observable, of } from 'rxjs';
import { ProductionLine } from 'src/app/types/productionLine';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductionlineService {
  // private productionLineUrl = 'https://mdferasmus.azurewebsites.net/api/production_line';
  private productionLineUrl = 'https://localhost:5001/api/production_line';


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private messageService: MessageService) { }

  getProductionLines(): Observable<ProductionLine[]> {
    return this.http.get<ProductionLine[]>(this.productionLineUrl)
    // .pipe(
    //     tap(_ => this.log('fetched machines')),
    //     catchError(this.handleError<Machine[]>('getMachines', []))
    //   );
  }

  getProductionLine(id: number): Observable<ProductionLine> {
    const url = `${this.productionLineUrl}/${id}`;
    return this.http.get<ProductionLine>(url)
    // .pipe(
    // tap(_ => this.log(`fetched machine id=${id}`)),
    // catchError(this.handleError<Machine>(`getMachine id=${id}`))
    // );
  }

  addProductionLine(productionLine: ProductionLine): Observable<ProductionLine> {
    const postRequest = this.http.post<ProductionLine>(this.productionLineUrl, productionLine, this.httpOptions);
    return postRequest.pipe(
      catchError(this.handleError<ProductionLine>('addProductionLine'))
    );
  }

  private handleError<T>(operationFail = 'operationFail', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}
