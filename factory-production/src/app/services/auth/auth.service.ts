import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  public getToken(): string {
    return localStorage.getItem("token");
  }

  public getUsersId(): string {
    return localStorage.getItem("id");
  }

  public isAdmin(): boolean {
    const isCustomer = localStorage.getItem("isCustomer")

    console.log("----------");
    console.log(isCustomer);
    console.log("----------");

    const isAdmin = (isCustomer == 'false');

    console.log(isAdmin);
    
    return isAdmin;
  }
}
