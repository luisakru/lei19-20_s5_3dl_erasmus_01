import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { MessageService } from "../message/message.service";
import { CUSTOMERS } from "src/app/mock-customer";
import { Customer } from "src/app/types/customer";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { CustomerSignIn } from "src/app/types/CustomerSignIn";
import { CustomerSignUp } from "src/app/types/CustomerSignUp";
import { AuthService } from "../auth/auth.service";
import { Order } from "src/app/types/order";
import { MatSnackBar } from '@angular/material/snack-bar';
import { MyOrderEditComponent } from 'src/app/components/om/customer-panel/orders/my-order-edit/my-order-edit.component';

@Injectable({
  providedIn: "root"
})
export class CustomerService {
  private customerUrl = "http://localhost:3000/customer";
  private orderUrl = "http://localhost:3000/order";
  private authorizationUrl = "http://localhost:3000/authorization";

  private token = localStorage.getItem("token");

  httpOptions = {
    headers: new HttpHeaders({
      Authorization: `${this.token}`,
      "Content-Type": "application/json"
    })
  };

  httpOptions2 = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private messageService: MessageService,
    private _snackBar: MatSnackBar
  ) { }

  getHttpOptions(): { headers: HttpHeaders } {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `${this.authService.getToken()}`,
      })
    };
    return httpOptions;
  }

  getCustomers(): Observable<Customer[]> {
    const httpOptions = this.getHttpOptions();
    return this.http.get<Customer[]>(this.customerUrl, httpOptions);
  }

  getCurrentCustomer(): Observable<Customer> {
    const url = this.customerUrl + "/get/" + this.authService.getUsersId();
    const httpOptions = this.getHttpOptions();
    return this.http.get<Customer>(url, httpOptions);
  }

  getCustomer(id: string): Observable<Customer> {
    const url = this.customerUrl + "/get/" + id;
    const httpOptions = this.getHttpOptions();
    return this.http.get<Customer>(url, httpOptions);
  }

  getCustomerOrders(): Observable<any> {
    const url = this.customerUrl + "/" + this.authService.getUsersId() + "/orders";
    const httpOptions = this.getHttpOptions();
    return this.http.get<Order[]>(url, httpOptions);
  }

  signIn(customerSignIn: CustomerSignIn): Observable<any> {
    const url = this.customerUrl + "/" + "signin";
    const request = this.http.post<Customer>(
      url,
      customerSignIn,
      this.httpOptions2
    );
    return request.pipe(catchError(this.handleError<Customer>("signIn")));
  }

  signUp(customerSignUp: CustomerSignUp): Observable<any> {
    const url = this.customerUrl + "/" + "signup";
    const postRequest = this.http.post<Customer>(
      url,
      customerSignUp,
      this.httpOptions2
    );
    return postRequest.pipe(catchError(this.handleError<Customer>("signUp")));
  }

  updateCustomer(customer: any): Observable<any> {
    const url = this.customerUrl + "/" + this.authService.getUsersId();
    const httpOptions = this.getHttpOptions();
    return this.http
      .put(url, customer, httpOptions)
      .pipe(catchError(this.handleError<any>("updateOperation")));
  }

  updateCustomerAdmin(customer: any): Observable<any> {
    const url = this.customerUrl + "/" + customer._id;
    const httpOptions = this.getHttpOptions();
    return this.http
      .put(url, customer, httpOptions)
      .pipe(catchError(this.handleError<any>("updateOperation")));
  }

  getAllOrders(): Observable<any> {
    const httpOptions = this.getHttpOptions();
    return this.http.get<Order[]>(this.orderUrl, httpOptions);
  }

  getOrder(id: string): Observable<any> {
    const url = this.orderUrl + "/" + id;
    const httpOptions = this.getHttpOptions();
    return this.http.get<Order>(url, httpOptions);
  }

  updateOrder(order: any): Observable<any> {
    const url = this.orderUrl + "/" + order._id;
    const httpOptions = this.getHttpOptions();
    return this.http
      .put(url, { quantity: order.quantity, deliveryDay: order.deliveryDay }, httpOptions)
      .pipe(catchError(this.handleErrorUpdateOrder<any>("updateOrder")));
  }

  cancelOrder(order: any): Observable<any> {
    const url = this.orderUrl + "/" + order._id;
    const httpOptions = this.getHttpOptions();
    return this.http
      .put(url, { status: 4 }, httpOptions)
      .pipe(catchError(this.handleError<any>("updateOrder")));
  }

  createOrder(p: any, q: any) {
    const httpOptions = this.getHttpOptions();
    const order = {
      quantity: q,
      product: p,
      customer: this.authService.getUsersId()
    } as Order;

    const postRequest = this.http.post<Order>(
      this.orderUrl,
      order,
      httpOptions
    );
    return postRequest.pipe(catchError(this.handleError<Order>("create order")));
  }

  getAuthorizationSettings(): Observable<any> {
    const url = this.authorizationUrl;
    const httpOptions = this.getHttpOptions();
    return this.http.get<any>(url, httpOptions);
  }

  updateAuthorizationSettings(value: any): Observable<any> {
    const url = this.authorizationUrl;
    const httpOptions = this.getHttpOptions();

    console.log("url: " + url);
    console.log(value);
    console.log(httpOptions);

    // TODO
    // Not sending any request to the server.
    // let request = this.http.put(url, value, this.httpOptions);
    let request = this.http.put<any>(url, value);
    return request.pipe(
      catchError(this.handleError('updateAuthorizationSettings', value))
    );
  }

  private handleError<T>(operationFail = "operationFail", result?: T) {
    return (error: any): Observable<T> => {
      this._snackBar.open("Error: " + operationFail, "OK", {
        duration: 5000,
        panelClass: ['snackbar']
      });
      return of(result as T);
    };
  }

  private handleErrorUpdateOrder<T>(operationFail = "operationFail", result?: T) {
    return (error: any): Observable<T> => {
      this._snackBar.open("Unauthorized", "Okay", {
        duration: 5000,
        panelClass: ['snackbar']
      });
      return of(result as T);
    };
  }

} 
