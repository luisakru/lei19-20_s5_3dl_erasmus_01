import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { MachineType } from '../../types/machinetype';
import { MessageService } from '../message/message.service';
import { Machine } from 'src/app/types/machine';
import { Operation } from 'src/app/types/operation';
import { OperationsList } from 'src/app/types/OperationsList';


@Injectable({ providedIn: 'root' })
export class MachineTypeService {

  // private machineTypeUrl = 'https://mdferasmus.azurewebsites.net/api/machinetype';  // URL to web api
  private machineTypeUrl = 'https://localhost:5001/api/machinetype';  // URL to web api


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET machine types from the server */
  getMachineTypes(): Observable<MachineType[]> {
    return this.http.get<MachineType[]>(this.machineTypeUrl)
      .pipe(
        tap(_ => this.log('fetched machinetypes')),
        catchError(this.handleError<MachineType[]>('getMachinetypes', []))
      );
  }

  /** GET machinetypes by id. Return `undefined` when id not found */
  getMachineTypeNo404<Data>(id: number): Observable<MachineType> {
    const url = `${this.machineTypeUrl}/?id=${id}`;
    return this.http.get<MachineType[]>(url)
      .pipe(
        map(mdf => mdf[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} machinetype id=${id}`);
        }),
        catchError(this.handleError<MachineType>(`getMachinetype id=${id}`))
      );
  }

  /** GET machinetype by id. Will 404 if id not found */
  getMachineType(id: number): Observable<MachineType> {
    const url = `${this.machineTypeUrl}/${id}`;
    return this.http.get<MachineType>(url).pipe(
      tap(_ => this.log(`fetched machinetype id=${id}`)),
      catchError(this.handleError<MachineType>(`getMachineType id=${id}`))
    );
  }

  /** POST: add a new machine type to the server */
  addMachineType(machinetype: MachineType): Observable<MachineType> {
    return this.http.post<MachineType>(this.machineTypeUrl, machinetype, this.httpOptions).pipe(
      tap((newMachinetype: MachineType) => this.log(`added machinetype w/ id=${newMachinetype.id}`)),
      catchError(this.handleError<MachineType>('addMachineType'))
    );
  }

  /** PUT: update the machine type on the server */
  updateMachineType(machinetype: MachineType, operationsList: OperationsList): Observable<any> {
    const url = `${this.machineTypeUrl}/${machinetype.id}`;
    return this.http.put(url, operationsList, this.httpOptions).pipe(
      tap(_ => this.log(`updated machinetype id=${machinetype.id}`)),
      catchError(this.handleError<any>('updateMachineType'))
    );
  }

  /** GET machinetypeOPERATIONS by id. Will 404 if id not found */
  getMachineTypeOperations(id: number): Observable<Operation[]> {
    const url = `${this.machineTypeUrl}/${id}/operations`;
    const a = this.http.get<Operation[]>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched machineTypeOperation id=${id}`)),
      catchError(this.handleError<Operation[]>(`getMachineTypeOperations id=${id}`))
    );
    return a
  }

  /** GET machinetypeOPERATIONS by id. Will 404 if id not found */
  getMachineTypeMachines(id: number): Observable<Machine[]> {
    const url = `${this.machineTypeUrl}/${id}/machines`;
    return this.http.get<Machine[]>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched machineTypeMachines id=${id}`)),
      catchError(this.handleError<Machine[]>(`getMachineTypeMachines id=${id}`))
    );
  }

  /**
   * Handle Http operationFail that failed.
   * Let the app continue.
   * @param operationFail - name of the operationFail that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operationFail = 'operationFail', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operationFail} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a MachineService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MachineTypeService: ${message}`);
  }

}

