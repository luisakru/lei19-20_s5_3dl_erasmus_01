import { TestBed } from '@angular/core/testing';

import { MachineTypeService } from './machinetype.service';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http'; 

describe('MachinetypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule, HttpClientModule],
  }));

  it('should be created', () => {
    const service: MachineTypeService = TestBed.get(MachineTypeService);
    expect(service).toBeTruthy();
  });
});
