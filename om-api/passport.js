const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local');

const { JWT_SECRET } = require('./src/config/index');
const Customer = require('./src/models/customerModel');

// JSON Web Token Strategy
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: JWT_SECRET
}, async (payload, done) => {
    try {
        // find customer specify in token
        const customer = await Customer.findById(payload.sub);

        // if customer doen't exist handle
        if (!customer) {
            return (done(null, false));
        }

        // otherwise return user
        done(null, customer);
    }
    catch (error) {
        done(error, false);
    }
}));

// Local Stretagy
passport.use(new LocalStrategy({
    usernameField: 'name'
}, async (name, password, done) => {
    try {
        // Find the customer by name
        const customer = await Customer.findOne({ name });

        // If not, handle it
        if (!customer) {
            return done(null, false);
        }

        // Check if the password is correct
        const isMatch = await customer.isValidPassword(password);

        // If not, handle it
        if (!isMatch) {
            return done(null, false);
        }

        // Otherwise, return the user
        done(null, customer);

    } catch (error) {
        done(error, false);
    }
}));