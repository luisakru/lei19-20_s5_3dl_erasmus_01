const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
mongoose.connect('mongodb://localhost/arqsi'); // necessary?
const Schema = mongoose.Schema;

const customerSchema = new Schema({
    name: {type: String, required: true, max: 140, unique: true},
    address: { type: String, required: true, max: 320 },
    password: { type: String, required: true },
    type_customer: { type: Boolean, default: true },
});

customerSchema.pre('save', async function(next) {
    try {
      // Generate a salt
      const salt = await bcrypt.genSalt(10);
      
      // Generate a password hash (salt + hash)
      const passwordHash = await bcrypt.hash(this.password, salt);

      // Re-assign hashed version over original, plain text password
      this.password = passwordHash;
        
      next();
    } catch(error) {
      next(error);
    }
  });

customerSchema.methods.isValidPassword = async function(newPassword) {
    try {
      return await bcrypt.compare(newPassword, this.password);
    } catch(error) {
      throw new Error(error);
    }
  }

// Create a model
const Customer = mongoose.model("customer", customerSchema);

// Export the model
module.exports = Customer;