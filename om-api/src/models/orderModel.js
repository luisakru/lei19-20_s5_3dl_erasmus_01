const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost/arqsi');
const codes = require('../restapi/order-status');

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    status: {type: codes, default: codes.in_process},
    quantity: {type: Number, min: 1, max: 2400, required: true},
    customer: { type: mongoose.Schema.Types.ObjectId, ref: 'customer', required: true },
    product: { type: Number, required: true },
    manufactoringPlanId: { type: Number, required: true },
    deliveryDay: {type: Date, min: Date.now},
}, {
    timestamps: {createdAt: 'timestamp', updatedAt: 'changed'}
});
  
module.exports = mongoose.model("order", orderSchema);