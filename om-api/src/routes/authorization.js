"use strict";
const express = require('express');
const passport = require('passport');
const codes = require('../restapi/http-codes');
const HttpError = require('../restapi/http-error.js');
const controller = require('../controllers/authorizationController');
const order = express.Router();
const passportJWT = passport.authenticate('jwt', { session: false });
const config = require('../config/authorization.json');
const editJsonFile = require("edit-json-file");

order.route('/')
    // .put(passportJWT, controller.updateAuthorization)
    // .get(passportJWT, controller.getSettings) 
    .put((req, res, next) => {
        let file = editJsonFile(process.cwd() + '/src/config/authorization.json');

        const category = req.body.category + ".customer";
        file.set(category, req.body.value);
        file.save();

        res.statusCode = 200;
        res.locals.processed = true;
        next();
    })
    .get((req, res, next) => {
        res.statusCode = 200;
        res.locals.processed = true;
        res.locals.items = config;
        next();
    })
    .all((req, res, next) => {
        if (res.locals.processed) {
            next();
        } else {
            // reply with status code "wrong method"  405
            next(new HttpError('This method is not allowed at ' + req.originalUrl, codes.wrongmethod));
        }
    });

/**
 * This middleware would finally send any data that is in res.locals to the client (as JSON) or, if nothing left, will send a 204.
 */
order.use((req, res, next) => {
    if (res.locals.items) {
        res.json(res.locals.items);
        delete res.locals.items;
    } else if (res.locals.processed) {
        if (res.get('Status-Code') == undefined) { // maybe other code has set a better status code before
            res.status(204); // no content;
        }
        res.end();
    } else {
        next(); // will result in a 404 from app.js
    }
});


module.exports = order;