"use strict";
const express = require('express');
const passport = require('passport');
const passportConf = require('../../passport');
const codes = require('../restapi/http-codes');
const HttpError = require('../restapi/http-error.js');
//const controller = require('../controllers/controller');
const controller = require('../controllers/userController');
const customer = express.Router();
const passportSignIn = passport.authenticate('local', { session: false });
const passportJWT = passport.authenticate('jwt', { session: false });

// customer
customer.route('/signup')
    .post(controller.signup)
    .all((req, res, next) => {
        if (res.locals.processed) {
            next();
        } else {
            // reply with status code "wrong method"  405
            next(new HttpError('This method is not allowed at ' + req.originalUrl, codes.wrongmethod));
        }
    });

// customer + admin
customer.route('/signin')
    .post(passportSignIn, controller.signin)
    .all((req, res, next) => {
        if (res.locals.processed) {
            next();
        } else {
            // reply with status code "wrong method"  405
            next(new HttpError('This method is not allowed at ' + req.originalUrl, codes.wrongmethod));
        }
    });
    
// admin
customer.route('/')
    .get(passportJWT, controller.selectAllcustomers)
    .all((req, res, next) => {
        if (res.locals.processed) {
            next();
        } else {
            // reply with status code "wrong method"  405
            next(new HttpError('This method is not allowed at ' + req.originalUrl, codes.wrongmethod));
        }
    });

// customer + admin
customer.route('/get/:id') // URL like this, because only /:id -> error
    .get(passportJWT, controller.selectcustomerId);

customer.route('/:id/orders') 
    .get(passportJWT, controller.getCustomerOrders);

// customer + admin
customer.route('/:id')
    .put(passportJWT, controller.updatecustomer) // no autentification, because both roles are allowed to change
    .all((req, res, next) => {
        if (res.locals.processed) {
            next();
        } else {
            // reply with status code "wrong method"  405
            next(new HttpError('This method is not allowed at ' + req.originalUrl, codes.wrongmethod));
        }
    });

/**
 * This middleware would finally send any data that is in res.locals to the client (as JSON) or, if nothing left, will send a 204.
 */
customer.use((req, res, next) => {
    if (res.locals.items) {
        res.json(res.locals.items);
        delete res.locals.items;
    } else if (res.locals.processed) {
        if (res.get('Status-Code') == undefined) { // maybe other code has set a better status code before
            res.status(204); // no content;
        }
        res.end();
    } else {
        next(); // will result in a 404 from app.js
    }
});


module.exports = customer;
