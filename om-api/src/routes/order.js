"use strict";
const express = require('express');
const passport = require('passport');
const passportConf = require('../../passport');
const codes = require('../restapi/http-codes');
const HttpError = require('../restapi/http-error.js');
//const controller = require('../controllers/controller');
const controller = require('../controllers/orderController');
const order = express.Router();
const passportJWT = passport.authenticate('jwt', { session: false });

order.route('/:id')
    .get(passportJWT, controller.selectOrderId) // customer + admin
    .put(passportJWT, controller.updateOrder) // admin **new*** + customer
    .all((req, res, next) => {
        if (res.locals.processed) {
            next();
        } else {
            // reply with status code "wrong method"  405
            next(new HttpError('This method is not allowed at ' + req.originalUrl, codes.wrongmethod));
        }
    });

order.route('/')
    .get(passportJWT, controller.selectAllOrders) // admin
    .post(passportJWT, controller.insertOrder) // customer
    .all((req, res, next) => {
        if (res.locals.processed) {
            next();
        } else {
            // reply with status code "wrong method"  405
            next(new HttpError('This method is not allowed at ' + req.originalUrl, codes.wrongmethod));
        }
    });

/**
 * This middleware would finally send any data that is in res.locals to the client (as JSON) or, if nothing left, will send a 204.
 */
order.use((req, res, next) => {
    if (res.locals.items) {
        res.json(res.locals.items);
        delete res.locals.items;
    } else if (res.locals.processed) {
        if (res.get('Status-Code') == undefined) { // maybe other code has set a better status code before
            res.status(204); // no content;
        }
        res.end();
    } else {
        next(); // will result in a 404 from app.js
    }
});


module.exports = order;