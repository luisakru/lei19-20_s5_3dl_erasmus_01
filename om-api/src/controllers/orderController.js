'use strict';
const HttpError = require('../restapi/http-error.js');
const OrderModel = require("../models/orderModel");
const https = require('https');
const config = require('../config/authorization.json')

var order_status = {
    1: "in_process",
    2: "in_delivery",
    3: "completed",
    4: "canceld",
};

exports.selectOrderId = (req, res, next) => {
    // AUTHENTICATION
    if (req.user.type_customer) {
        if (!config.consultOrder.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.consultOrder.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    let query = OrderModel.findById(req.params.id);
    query
        .populate('customer')
        .exec()
        .catch(err => {
            next(HttpError("No orders in database.", 404))
        })
        .then(item => {
            if (!item) {
                res.status(204).end();
            } else {
                item.status = order_status[item.status];
                res.locals.items = item;
                res.locals.processed = true;
                res.status(200);
                next();
            }
        })
};

exports.updateOrder = (req, res, next) => {
     // AUTHENTICATION
     if (req.user.type_customer) {
        if (!config.updateOrder.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.updateOrder.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    let query = OrderModel.findByIdAndUpdate(req.params.id, req.body, { runValidators: true, new: true });
    query.exec()
        .catch(err => {
            if (err.errors) {
                return next(new HttpError(err.message, 400));
            } else {
                return next(HttpError("Error occured in database.", 500));
            }
        })
        .then(item => {
            if (!item) {
                res.status(204).end();
            } else {

                // if (JSON.stringify(req.body) == '{"status":4}') {
                //     const url = "https://localhost:5003/api/product/decrease/" + item.product;
                //     putProduct(url);

                //     const urlQuantity = "https://localhost:5003/api/product/quantity/" + item.product;
                //     const quantityJSON = JSON.stringify({
                //         quantity: item.quantity * -1
                //     })
                //     putProductData(urlQuantity, quantityJSON);
                // }

                item.status = order_status[item.status];
                res.locals.items = item;
                res.locals.processed = true;
                res.status(200);
                next();
            }
        });
};

exports.selectAllOrders = (req, res, next) => {
    // AUTHENTICATION
    if (req.user.type_customer) {
        if (!config.consultAllOrders.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.consultAllOrders.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    let query = OrderModel.find({});
    query.exec()
        .catch(err => {
            next(HttpError("No orders in database.", 404))
        })
        .then(items => {
            if (!items) {
                res.status(204).end();
            } else {
                const item_list = [];
                items.forEach(item => {
                    const newItem = item;
                    newItem.status = order_status[item.status];
                    item_list.push(item)
                })
                res.locals.items = item_list;
                res.locals.items = items;
                res.locals.processed = true;
                res.status(200);
                next();
            }
        })
};

// const putProduct = async (url) => {
//     const jsonData = JSON.stringify({})

//     const options = {
//         hostname: 'localhost',
//         port: 5003,
//         path: url,
//         method: 'PUT',
//         rejectUnauthorized: false
//     }

//     const req = https.request(options, (res) => {
//         res.on('jsonData', (d) => {
//             process.stdout.write(d)
//         })
//     })

//     req.on('error', (error) => {
//         console.error(error)
//     })

//     req.write(jsonData)
//     req.end()
// }

// const putProductData = async (url, data) => {

//     const options = {
//         hostname: 'localhost',
//         port: 5003,
//         path: url,
//         method: 'PUT',
//         rejectUnauthorized: false,
//         headers: {
//             "Content-Type": "application/json"
//         }
//     }

//     const req = https.request(options, (res) => {
//         res.on('data', (d) => {
//             process.stdout.write(d)
//         })
//     })

//     req.on('error', (error) => {
//         console.error(error)
//     })

//     req.write(data)
//     req.end()
// }

exports.insertOrder = (req, res, next) => {
    // AUTHENTICATION
    if (req.user.type_customer) {
        if (!config.insertOrder.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.insertOrder.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    const newOrder = req.body;
    newOrder['manufactoringPlanId'] = 1;

    let order = new OrderModel(newOrder);
    order.save(err => {
        if (err) {
            return next(err);
        }

        // const urlIncrease = "https://localhost:5003/api/product/increase/" + req.body.product;
        // putProduct(urlIncrease);

        // const urlQuantity = "https://localhost:5003/api/product/quantity/" + req.body.product;
        // const quantityJSON = JSON.stringify({
        //     quantity: req.body.quantity
        // })
        // putProductData(urlQuantity, quantityJSON);

        order.status = order_status[order.status];
        res.locals.items = order;
        res.locals.processed = true;
        res.status(201);
        next();
    })
};