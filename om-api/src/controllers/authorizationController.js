'use strict';
const HttpError = require('../restapi/http-error.js');
const OrderModel = require("../models/orderModel");
const https = require('https');
const { JWT_SECRET } = require("../config/index");
const config = require('../config/authorization.json')

exports.updateAuthorization = (req, res, next) => {
       // AUTHENTICATION ONLY ADMIN 
       if (req.user.type_customer) {
              return res.status(401).json({ message: 'Unauthorized' });
       }

       let file = editJsonFile(process.cwd() + '/src/config/authorization.json');

       const category = req.body.category + ".customer";
       file.set(category, req.body.value);
       file.save();

       res.statusCode = 200;
       res.locals.processed = true;
       next();

};

exports.getSettings = (req, res, next) => {
       // AUTHENTICATION ONLY ADMIN 
       if (req.user.type_customer) {
              return res.status(401).json({ message: 'Unauthorized' });
       }

       res.statusCode = 200;
       res.locals.processed = true;
       res.locals.items = config;
       next();
};