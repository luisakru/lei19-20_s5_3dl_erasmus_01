'use strict';
const JWT = require('jsonwebtoken');

const HttpError = require('../restapi/http-error.js');
const customerModel = require("../models/customerModel");
const OrderModel = require("../models/orderModel");
const { JWT_SECRET } = require("../config/index");
const config = require('../config/authorization.json')

function signToken(customer) {
    return JWT.sign({
        sub: customer._id,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + 1) // current day + 1 day
    }, JWT_SECRET);
}

exports.signup = (req, res, next) => {
    let customer = new customerModel(req.body);
    console.log(req.body);
    customer.save(err => {
        console.log("err: ");
        console.log(err);
        if (err) {
            return next(err);
        }

        // generate token
        const token = signToken(customer);

        res.locals.items = { customer, token };
        res.locals.processed = true;
        res.status(201);
        next();
    })
};

exports.signin = (req, res, next) => {
    const token = signToken(req.user);
    const user = req.user;
    res.locals.items = { user, token };
    res.locals.processed = true;
    res.status(201);
    next();
};

exports.selectcustomerId = (req, res, next) => {
    // AUTHENTICATION
    if (req.user.type_customer) {
        if (!config.consultCustomer.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.consultCustomer.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    let query = customerModel.findById(req.params.id);
    query.exec()
        .catch(err => {
            next(HttpError("No customers in database.", 404))
        })
        .then(items => {
            if (!items) {
                res.status(204).end();
            } else {
                res.locals.items = items;
                res.locals.processed = true;
                res.status(200);
                next();
            }
        })
};

exports.updatecustomer = (req, res, next) => {
    // AUTHENTICATION
    if (req.user.type_customer) {
        if (!config.updateCustomer.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.updateCustomer.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    let query = customerModel.findByIdAndUpdate(req.params.id, req.body, { runValidators: true, new: true });
    query.exec()
        .catch(err => {
            if (err.errors) {
                return next(new HttpError(err.message, 400));
            } else {
                return next(HttpError("Error occured in database.", 500));
            }
        })
        .then(items => {
            if (!items) {
                res.status(204).end();
            } else {
                res.locals.items = items;
                res.locals.processed = true;
                res.status(200);
                next();
            }
        });
};

exports.selectAllcustomers = (req, res, next) => {
    // AUTHENTICATION
    if (req.user.type_customer) {
        if (!config.consultAllCustomers.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.consultAllCustomers.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    let query = customerModel.find({});
    query.exec()
        .catch(err => {
            next(HttpError("No customer in database.", 404))
        })
        .then(items => {
            if (!items) {
                res.status(204).end();
            } else {
                const only_customers = [];
                items.forEach(item => {
                    if (item.type_customer) {
                        only_customers.push(item);
                    }
                })
                res.locals.items = only_customers;
                res.locals.processed = true;
                res.status(200);
                next();
            }
        })
};

exports.getCustomerOrders = (req, res, next) => {
    // AUTHENTICATION
    if (req.user.type_customer) {
        if (!config.consultAllCustomerOrders.customer)
            return res.status(401).json({ message: 'Unauthorized' });
    }
    else {
        if (!config.consultAllCustomerOrders.admin)
            return res.status(401).json({ message: 'Unauthorized' });
    }

    let query = OrderModel.find({});
    query
        .populate('customer')
        .exec()
        .catch(err => {
            next(HttpError("No orders in database.", 404))
        })
        .then(items => {
            if (!items) {
                res.status(204).end();
            } else {
                const customer_items = [];
                items.forEach(item => {
                    if (item.customer.equals(req.params.id)) {
                        customer_items.push(item);
                    }
                });
                res.locals.items = customer_items;
                res.locals.processed = true;
                res.status(200);
                next();
            }
        })

};