const app = require("../app");
const chai = require("chai");
const chaiHttp = require("chai-http");
const faker = require('faker');

const { expect } = chai;
chai.use(chaiHttp);

let token;
let customerId;
let orderId;

describe("Customers route", () => {
    const signup = '/customer/signup';
    const signin = '/customer/signin';
    const order = '/order';
    const customer = {
        name: faker.internet.userName(),
        address: faker.address.streetAddress("###"),
        password: faker.internet.password()
    };


    describe('Signup', () => {
        it("Successful sign up", done => {
            chai
                .request(app)
                .post(signup)
                .send(customer)
                .end((err, res) => {
                    expect(res).to.have.status(201);
                    expect(res.body).not.to.be.empty;
                    expect(res.body).to.have.property('token');
                    token = res.body.token;
                    customerId = res.body.customer._id;
                    console.log('---------------');
                    console.log(customerId);
                    console.log('-----------------');

                    expect(res.body).to.have.property('customer');
                    done();
                });
        });

        it("Should return 500 if duplicated name", done => {
            chai
                .request(app)
                .post(signup)
                .send(customer)
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    done();
                });
        });
    });

    describe('Signin', () => {
        it('Should return error 400 if user email and password empty', done => {
            let user = {};
            chai
                .request(app)
                .post(signin)
                .send(user)
                .end((err, res) => {
                    expect(res.status).to.be.equal(400);
                    done();
                });
        });

        it('Successful sign in and should return our token', done => {
            chai
                .request(app)
                .post(signin)
                .send({ name: customer.name, password: customer.password })
                .end((err, res) => {
                    expect(res.status).to.be.equal(201);
                    expect(res.body).not.to.be.empty;
                    expect(res.body).to.have.property('token');
                    token = res.body.token;
                    done();
                });
        });
    });


    describe('Create order', () => {
        it('Should return 201 and order if it was created', done => {
            const orderObject = {
                status: 1,
                quantity: 300,
                product: 1,
                customer: customerId
            }
            chai
                .request(app)
                .post(order)
                .set('Authorization', token)
                .send(orderObject)
                .end((err, res) => {
                    expect(res.status).to.be.equal(201);
                    orderId = res.body._id;
                    expect(res.body).not.to.be.empty;
                    expect(res.body).to.have.property('quantity');
                    expect(res.body).to.have.property('status');
                    done();
                });
        });

        it('Should return 401', done => {
            const orderObject = {
                status: 1,
                quantity: 300,
                product: 1,
                customer: customerId
            }
            chai
                .request(app)
                .post(order)
                .send(orderObject)
                .end((err, res) => {
                    expect(res.status).to.be.equal(401);
                    done();
                });
        });
    });

    describe('Get order by id', () => {
        it('Should return 200 and order', done => {
            chai
                .request(app)
                .get(order + '/' + orderId)
                .set('Authorization', token)
                .end((err, res) => {
                    expect(res.status).to.be.equal(200);
                    expect(res.body).not.to.be.empty;
                    expect(res.body).to.have.property('quantity');
                    expect(res.body).to.have.property('status');
                    done();
                });
        });

        it('Should return 401', done => {
            chai
                .request(app)
                .get(order + '/' + orderId)
                .end((err, res) => {
                    expect(res.status).to.be.equal(401);
                    done();
                });
        });
    });

});