"use strict";

const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
// const requestLogger = require('morgan');

// own modules
const HttpError = require('./src/restapi/http-error.js');
const restAPIchecks = require('./src/restapi/request-checks.js');
const customer = require('./src/routes/customer');
const order = require('./src/routes/order');
const authorization = require('./src/routes/authorization');

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/arqsi", { // "mongodb://localhost/arqsi 27017"
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// app creation
const app = express();

app.use(cors());

// Middlewares *************************************************
// configure app to use bodyParser()
// this will let us get the data from a POST
//app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// logging
app.use((req, res, next) => {
    console.log(`Request of type ${req.method} to URL ${req.originalUrl}`);
    next();
});

// API request checks for API-version and JSON etc.
//app.use(restAPIchecks);


// Routes ******************************************************
app.use('/customer', customer);
app.use('/order', order);
app.use('/authorization', authorization);

// Errorhandling and requests without proper URLs ************************
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    debug('Catching unmatched request to answer with 404');
    const err = new HttpError('Not Found', 404);
    next(err);
});


// error handlers (express recognizes it by 4 parameters!)
// development error handler
// will print stacktrace as JSON response
app.use((err, req, res, next) => {
    let errorInformation = {
        error: {
            statuscode: err.status,
            message: err.message,
            path: req.path,
            error: err.stack.toString()
        }
    };
    console.log(`Error to send: ${JSON.stringify(errorInformation, null, 2)}`); // if you like remove this
    res.status(err.status || 500);
    res.json(errorInformation);
});



// Start server ****************************
app.listen(3000, (err) => {
    if (err !== undefined) {
        console.log(`Error on startup: ${err}`);
    } else {
        console.log('Listening on port 3000');
    }
});




module.exports = app;