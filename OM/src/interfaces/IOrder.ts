import { IUser } from "./IUser"


export interface IOrder {
  _id: string;
  status: String;
  quantity: Number;
  orderedBy: IUser;
  // product: IProduct;
}

export interface IOrderInputDTO {
  status: String;
  quantity: Number;
  orderedBy: IUser;
  // product: IProduct;
}