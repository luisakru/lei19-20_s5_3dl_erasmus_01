"use strict";
module.exports = {
    in_process: 1,
    in_delivery: 2,
    completed: 3,
    cancelled: 4,
};