import { IOrder } from '../interfaces/IOrder';
const codes = require('./order-status');
import mongoose from 'mongoose';

const Order = new mongoose.Schema(
  {
    status: {
      type: codes,
      default: codes.in_process,
    },

    quantity: {
      type: Number,
      min: 1,
      max: 2400,
      default: null,
      required: [true, 'Please enter quantity'],
    },

    orderedBy: {
      type: codes,
      ref: 'Costumer',
    },

    //product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product'}

  },
  { timestamps: true },
);

export default mongoose.model<IOrder & mongoose.Document>('Order', Order);
