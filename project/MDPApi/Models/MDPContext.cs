using Microsoft.EntityFrameworkCore;

namespace MDPApi.Models
{
    public class MDPContext : DbContext
    {
        public MDPContext(DbContextOptions<MDPContext> options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<ManufactoringPlan> ManufactoringPlan { get; set; }
    }
}