namespace MDPApi.Models
{
    public class ManufactoringPlanDTO
    {
        public long Id { get; set; }
        public string Description { get; set; }
    }
}