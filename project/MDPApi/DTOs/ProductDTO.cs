namespace MDPApi.DTOs
{
    public class ProductDTO
    {
        public long Id { get; set; }
        public long ManufactoringPlanId { get;  set; }
    }
}