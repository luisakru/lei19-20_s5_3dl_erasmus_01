using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDPApi.Models;
using MDPApi.DTOs;
using Microsoft.AspNetCore.Cors;

namespace MDPApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("IT3Client")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly MDPContext _context;

        public ProductController(MDPContext context)
        {
            _context = context;

            if (_context.Products.Count() == 0)
            {
                ManufactoringPlan plan1 = new ManufactoringPlan { Description = "plan desc" };
                ManufactoringPlan plan2 = new ManufactoringPlan { Description = "pierogi" };
                _context.ManufactoringPlan.AddRange(new List<ManufactoringPlan> { plan1, plan2 });
                _context.SaveChanges();

                Product product1 = new Product { 
                    ManufactoringPlanId = plan1.Id, 
                };

                Product product2 = new Product { 
                    ManufactoringPlanId = plan2.Id, 
                };

                Product product3 = new Product { 
                    ManufactoringPlanId = plan1.Id, 
                };

                _context.Products.Add(product1);
                _context.Products.Add(product2);
                _context.Products.Add(product3);
                _context.SaveChanges();
            }

        }


        [HttpGet()]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProducts()
        {
            var products = await _context.Products.ToListAsync();
            if (products.Count() == 0)
                return new List<ProductDTO>();

            var productsDTOs = new List<ProductDTO>();
            foreach (var product in products)
                productsDTOs.Add(new ProductDTO { 
                    Id = product.Id, 
                    ManufactoringPlanId = product.ManufactoringPlanId, 
                });


            return productsDTOs;
        }

        [HttpGet("{id}")]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<ProductDTO>> GetProduct(long id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            var productDTO = new ProductDTO { 
                Id = product.Id, 
                ManufactoringPlanId = product.ManufactoringPlanId, 
            };

            return productDTO;
        }

        [HttpGet("{id}/manufactoringplan")]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<ManufactoringPlanDTO>> GetProductManufactoringPlan(long id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return BadRequest();
            }

            var manufactoringPlan = _context.ManufactoringPlan.First(m => m.Id == product.ManufactoringPlanId);
            if (manufactoringPlan == null)
            {
                return NotFound();
            }

            var manufactoringPlanDTO = new ManufactoringPlanDTO { Id = manufactoringPlan.Id, Description = manufactoringPlan.Description };
            // TODO operations
            return manufactoringPlanDTO;
        }

        [HttpGet("/api/manufactoringplan")]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<IEnumerable<ManufactoringPlanDTO>>> GetManufactoringplas()
        {
            var mp = await _context.ManufactoringPlan.ToListAsync();
            if (mp.Count() == 0)
                return new List<ManufactoringPlanDTO>();

            var manufactoringPlanDTOs = new List<ManufactoringPlanDTO>();
            foreach (var manupl in mp)
                manufactoringPlanDTOs.Add(new ManufactoringPlanDTO { Id = manupl.Id, Description = manupl.Description });

            return manufactoringPlanDTOs;
        }



        [HttpPost()]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<Product>> PostProduct(Product item)
        {
            ManufactoringPlan manufactoringPlan = _context.ManufactoringPlan.First(mp => mp.Id == item.ManufactoringPlanId);
            if (manufactoringPlan == null)
            {
                return BadRequest();
            }

            _context.Products.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetProducts), new { id = item.Id }, item);
        }

    }
}