using System.Collections.Generic;

namespace MDFApi.Models
{
    public class OperationsList
    {
        public List<long> Ids { get; set; }
    }
}
