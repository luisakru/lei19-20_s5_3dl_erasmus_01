using System.Collections.Generic;

namespace MDFApi.Models
{
    public class Operation
    {
        public long Id { get; set; }
        public int Duration { get; set; }
        public ICollection<MachineTypeOperation> MachineTypeOperation { get; set; }
        // public Operation(int duration)
        // {
        //     Duration = duration;
        //     MachineTypeOperation = new List<MachineTypeOperation>();
        // }
    }
}