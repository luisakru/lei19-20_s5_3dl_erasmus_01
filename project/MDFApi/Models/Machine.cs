namespace MDFApi.Models
{
    public class Machine
    {
        public long Id { get; set; }
        public int Position { get; set; }
        public long MachineTypeId { get; set; }
        public long ProductionLineId { get; set; }
        public bool IsActive { get; set; }
    }
}