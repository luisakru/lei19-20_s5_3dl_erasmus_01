namespace MDFApi.Models
{
    public class MachineTypeOperation
    {
        public long MachineTypeId { get; set; }
        public MachineType MachineType { get; set; }

        public long OperationId { get; set; }
        public Operation Operation { get; set; }

        public MachineTypeOperation()
        {}
        public MachineTypeOperation(Operation operation, MachineType machineType)
        {
            Operation = operation;
            MachineType = machineType;
        }
    }
}
