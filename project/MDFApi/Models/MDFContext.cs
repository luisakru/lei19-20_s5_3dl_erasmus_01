using Microsoft.EntityFrameworkCore;

namespace MDFApi.Models
{
    public class MDFContext : DbContext
    {
        public MDFContext(DbContextOptions<MDFContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MachineTypeOperation>().HasKey(mo => new { mo.MachineTypeId, mo.OperationId });
            modelBuilder.Entity<MachineTypeOperation>()
                .HasOne(mo => mo.MachineType)
                .WithMany(mt => mt.MachineTypeOperation)
                .HasForeignKey(mo => mo.MachineTypeId);
            modelBuilder.Entity<MachineTypeOperation>()
                .HasOne(mo => mo.Operation)
                .WithMany(c => c.MachineTypeOperation)
                .HasForeignKey(mo => mo.OperationId);
        }

        public DbSet<Machine> Machines { get; set; }
        public DbSet<MachineType> MachineTypes { get; set; }
        public DbSet<MachineTypeOperation> MachineTypeOperations { get; set; }
        public DbSet<Operation> Operations { get; set; }
        public DbSet<ProductionLine> ProductionLines { get; set; }
    }
}