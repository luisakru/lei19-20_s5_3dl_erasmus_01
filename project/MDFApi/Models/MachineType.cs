using System.Collections.Generic;

namespace MDFApi.Models
{
    public class MachineType
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public ICollection<MachineTypeOperation> MachineTypeOperation { get; set; }
    }
}