﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDFApi.Migrations
{
    public partial class initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Machines_ProductionLines_ProductionLineId",
                table: "Machines");

            migrationBuilder.DropIndex(
                name: "IX_Machines_ProductionLineId",
                table: "Machines");

            migrationBuilder.AlterColumn<long>(
                name: "ProductionLineId",
                table: "Machines",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "ProductionLineId",
                table: "Machines",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.CreateIndex(
                name: "IX_Machines_ProductionLineId",
                table: "Machines",
                column: "ProductionLineId");

            migrationBuilder.AddForeignKey(
                name: "FK_Machines_ProductionLines_ProductionLineId",
                table: "Machines",
                column: "ProductionLineId",
                principalTable: "ProductionLines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
