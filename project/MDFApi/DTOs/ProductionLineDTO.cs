using System.Collections.Generic;

namespace MDFApi.DTOs
{
    public class ProductionLineDTO
    {
        public long Id { get; set; }
        public List<long> MachinesIds { get; set; }
        public ProductionLineDTO() { }
        public ProductionLineDTO(long id, List<long> machinesIds)
        {
            Id = id;
            MachinesIds = machinesIds;
        }
    }
}