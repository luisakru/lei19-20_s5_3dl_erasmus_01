using System.Collections.Generic;
using MDFApi.Models;

namespace MDFApi.DTOs
{
    public class OperationDTO
    {
        public long Id { get; set; }
        public int Duration { get; set; }
        public List<long> MachineTypeIds { get; set; }
        public OperationDTO(long id, int duration, List<long> machineTypesIds)
        {
            Id = id;
            Duration = duration;
            MachineTypeIds = machineTypesIds;
        }
    }
}