namespace MDFApi.DTOS
{
    public class MachineDTO
    {
        public long Id { get; set; }
        public int Position { get; set; }
        public long MachineTypeId { get; set; }
        public long ProductionLineId { get; set; }
        public bool IsActive { get; set; }
        public MachineDTO(long id, int position, long machineTypeId, long productionLineId, bool isActive)
        {
            Id = id;
            Position = position;
            MachineTypeId = machineTypeId;
            ProductionLineId = productionLineId;
            IsActive = isActive;
        }
    }
}