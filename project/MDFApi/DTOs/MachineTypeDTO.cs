using System.Collections.Generic;
using MDFApi.Models;

namespace MDFApi.DTOs
{
    public class MachineTypeDTO
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public List<long> OperationsIds { get; set; }
        public MachineTypeDTO(long id, string description, List<long> operationsIds)
        {
            Id = id;
            Description = description;
            OperationsIds = operationsIds;
        }
    }
}