using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDFApi.Models;
using MDFApi.DTOS;
using Microsoft.AspNetCore.Cors;

namespace MDFApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("IT3Client")]
    [ApiController]
    public class MachineController : ControllerBase
    {
        private readonly MDFContext _context;

        public MachineController(MDFContext context)
        {
            _context = context;

            if (_context.Machines.Count() == 0)
            {
                new MDFController(context);
                _context.SaveChanges();
            }
        }

        [HttpGet()]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<IEnumerable<MachineDTO>>> GetMachines()
        {
            var machines = await _context.Machines.ToListAsync();
            if (machines.Count() == 0)
            {
                return NotFound();
            }

            var machineDTOs = new List<MachineDTO>();
            foreach (var machine in machines)
            {
                machineDTOs.Add(new MachineDTO(machine.Id, machine.Position, machine.MachineTypeId, machine.ProductionLineId, machine.IsActive));
            }

            return machineDTOs;
        }

        [HttpGet("{id}")]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<MachineDTO>> GetMachine(long id)
        {
            var machine = await _context.Machines.FindAsync(id);
            if (machine == null)
            {
                return NotFound();
            }

            var machineDTO = new MachineDTO(machine.Id, machine.Position, machine.MachineTypeId, machine.ProductionLineId, machine.IsActive);

            return machineDTO;
        }

        [HttpPost()]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<Machine>> PostMachine(MachineDTO item)
        {
            MachineType machineType = _context.MachineTypes.First(mt => mt.Id == item.MachineTypeId);
            if (machineType == null)
            {
                return BadRequest();
            }

            Machine machine = new Machine { Position = item.Position, MachineTypeId = machineType.Id };
            _context.Machines.Add(machine);
            await _context.SaveChangesAsync();

            item.Id = machine.Id;

            return CreatedAtAction(nameof(GetMachine), new { id = machine.Id }, item);
        }

        [HttpPut("{id}")]
        [EnableCors("IT3Client")]
        public async Task<IActionResult> PutMachines(long id, Machine item)
        {
            MachineType machineType = _context.MachineTypes.First(mt => mt.Id == item.MachineTypeId);
            if (machineType == null)
            {
                return BadRequest();
            }

            var machine = _context.Machines.First(m => m.Id == id);
            if (machine == null)
            {
                return NotFound();
            }

            machine.MachineTypeId = item.MachineTypeId;
            machine.IsActive = item.IsActive;
            _context.Machines.Update(machine);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        [EnableCors("IT3Client")]
        public async Task<IActionResult> DeleteMachine(long id)
        {
            var machineItem = await _context.Machines.FindAsync(id);

            if (machineItem == null)
            {
                return NotFound();
            }

            _context.Machines.Remove(machineItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
