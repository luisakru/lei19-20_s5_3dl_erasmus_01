using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using MDFApi.Models;

namespace MDFApi.Controllers
{
    [Route("api/mdf")]
    [ApiController]
    public class MDFController : ControllerBase
    {
        private readonly MDFContext _context;

        public MDFController(MDFContext context)
        {
            _context = context;

            if (_context.Operations.Count() == 0)
            {
                Operation operation1 = new Operation { Duration = 20, MachineTypeOperation = null };
                Operation operation2 = new Operation { Duration = 10, MachineTypeOperation = null };
                MachineType machineType1 = new MachineType { Description = "test", MachineTypeOperation = null };
                MachineType machineType2 = new MachineType { Description = "test2", MachineTypeOperation = null };

                MachineTypeOperation machineTypeOperation1 = new MachineTypeOperation(operation1, machineType1);

                Machine machine1 = new Machine { Position = 10, MachineTypeId = 1 };
                Machine machine2 = new Machine { Position = 2, MachineTypeId = 2 };
                var ids = new List<Machine>();
                ids.Add(machine1);
                ProductionLine productionLine = new ProductionLine { };

                _context.Operations.AddRange(new List<Operation> { operation1, operation2 });
                _context.MachineTypes.AddRange(new List<MachineType> { machineType1, machineType2 });
                _context.Machines.AddRange(new List<Machine> { machine1, machine2 });
                _context.ProductionLines.AddRange(new List<ProductionLine> { productionLine });
                _context.SaveChanges();
            }
        }
    }
}
