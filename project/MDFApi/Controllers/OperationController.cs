using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using MDFApi.Models;
using MDFApi.DTOs;
using System.Linq;
using Microsoft.AspNetCore.Cors;

namespace MDFApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("IT3Client")]
    [ApiController]
    public class OperationController : ControllerBase
    {
        private readonly MDFContext _context;

        public OperationController(MDFContext context)
        {
            _context = context;

            if (_context.Operations.Count() == 0)
            {
                new MDFController(context);
                _context.SaveChanges();
            }
        }

        [HttpGet()]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<IEnumerable<OperationDTO>>> GetOperations()
        {
            var operationDTOs = new List<OperationDTO>();
            var operations = new List<Operation>();
            operations = await _context.Operations.ToListAsync();
            // var machineTypes = new List<MachineType>();
            // machineTypes = await _context.MachineTypes.Where(machine)
            operations.ForEach((operation) =>
            {
                operationDTOs.Add(new OperationDTO(operation.Id, operation.Duration, new List<long>()));
            }
            );

            return operationDTOs;
        }

        [HttpGet("{id}")]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<OperationDTO>> GetOperation(long id)
        {
            var operation = await _context.Operations.FindAsync(id);
            if (operation == null)
            {
                return NotFound();
            }

            var machineTypeOperations = await _context.MachineTypeOperations.Where(mto => mto.OperationId == operation.Id).ToListAsync();
            var machineTypesIds = new List<long>();
            foreach (var machineTypeOperation in machineTypeOperations)
            {
                machineTypesIds.Add(machineTypeOperation.MachineTypeId);
            }

            var operationDTO = new OperationDTO(operation.Id, operation.Duration, machineTypesIds);
            return operationDTO;
        }

        [HttpPost()]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<Operation>> PostOperation(OperationDTO item)
        {
            Operation operation = new Operation { Duration = item.Duration };

            List<MachineType> machineTypes = _context.MachineTypes.Where(mt => item.MachineTypeIds.Contains(mt.Id)).ToList();
            if(machineTypes.Count() == 0)
            {
                return BadRequest();
            }
            List<MachineTypeOperation> machineTypeOperations = new List<MachineTypeOperation>();
            foreach (var machineType in machineTypes)
            {
                var machineTypeOperation = new MachineTypeOperation(operation, machineType);
                machineTypeOperations.Add(machineTypeOperation);
                _context.MachineTypeOperations.Add(machineTypeOperation);
            }

            operation.MachineTypeOperation = machineTypeOperations;
            _context.Operations.Add(operation);
            await _context.SaveChangesAsync();
            item.Id = operation.Id;

            return CreatedAtAction(nameof(GetOperation), new { id = operation.Id }, item);
        }

        [HttpPut("{id}")]
        [EnableCors("IT3Client")]
        public async Task<IActionResult> PutOperation(long id, Operation item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        [EnableCors("IT3Client")]
        public async Task<IActionResult> DeleteOperation(long id)
        {
            var operationItem = await _context.Operations.FindAsync(id);

            if (operationItem == null)
            {
                return NotFound();
            }

            _context.Operations.Remove(operationItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
