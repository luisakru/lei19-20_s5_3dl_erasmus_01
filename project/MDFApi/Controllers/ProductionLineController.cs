using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDFApi.Models;
using MDFApi.DTOs;

namespace MDFApi.Controllers
{
    [Route("api/production_line")]
    [ApiController]
    public class ProductionLineController : ControllerBase
    {
        private readonly MDFContext _context;

        public ProductionLineController(MDFContext context)
        {
            _context = context;

            if (_context.ProductionLines.Count() == 0)
            {
                new MDFController(context);
                _context.SaveChanges();
            }
        }

        [HttpGet()]
        public async Task<ActionResult<IEnumerable<ProductionLineDTO>>> GetProductionLines()
        {
            var productionLines = await _context.ProductionLines.ToListAsync();
            if (productionLines.Count() == 0)
            {
                return new List<ProductionLineDTO>();
            }

            var productionLineDTOs = new List<ProductionLineDTO>();
            foreach (var productionLine in productionLines)
            {
                var machines = new List<long>() { };
                productionLineDTOs.Add(new ProductionLineDTO(productionLine.Id, machines));
            }

            return productionLineDTOs;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductionLineDTO>> GetProductionLine(long id)
        {
            var productionLine = await _context.ProductionLines.FindAsync(id);

            if (productionLine == null)
            {
                return NotFound();
            }

            var machines = await _context.Machines.Where(m => m.ProductionLineId == productionLine.Id).ToListAsync();
            var machineIds = new List<long> { };
            foreach (var machine in machines)
            {
                machineIds.Add(machine.Id);
            }
            var productionLineDTO = new ProductionLineDTO(productionLine.Id, machineIds);
            return productionLineDTO;
        }

        [HttpPost()]
        public async Task<ActionResult<ProductionLine>> PostProductionLine(ProductionLineDTO item)
        {
            var machines = new List<Machine> { };
            foreach (var machineId in item.MachinesIds)
            {
                var machine = _context.Machines.First(m => m.Id == machineId);
                if (machine == null)
                {
                    return BadRequest();
                }
                machines.Add(machine);
            }

            var productionLine = new ProductionLine { Id = item.Id };
            _context.ProductionLines.Add(productionLine);
            await _context.SaveChangesAsync();

            foreach (var machine in machines)
            {
                machine.ProductionLineId = productionLine.Id;
                _context.Machines.Update(machine);
            }
            await _context.SaveChangesAsync();
            item.Id = productionLine.Id;

            return CreatedAtAction(nameof(GetProductionLines), new { id = item.Id }, item);
        }
    }
}
