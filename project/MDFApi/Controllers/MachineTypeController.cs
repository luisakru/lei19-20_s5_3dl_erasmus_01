using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MDFApi.Models;
using MDFApi.DTOs;
using MDFApi.DTOS;
using Microsoft.AspNetCore.Cors;

namespace MDFApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("IT3Client")]
    [ApiController]
    public class MachineTypeController : ControllerBase
    {
        private readonly MDFContext _context;

        public MachineTypeController(MDFContext context)
        {
            _context = context;
            if (_context.MachineTypes.Count() == 0)
            {
                new MDFController(context);
                _context.SaveChanges();
            }
        }

        [HttpGet()]
        public async Task<ActionResult<IEnumerable<MachineType>>> GetMachineTypes()
        {
            // TODO get DTOs
            return await _context.MachineTypes.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MachineTypeDTO>> GetMachineType(long id)
        {
            var machineType = await _context.MachineTypes.FindAsync(id);

            if (machineType == null)
            {
                return NotFound();
            }

            var machineTypeOperations = await _context.MachineTypeOperations.Where(mto => mto.MachineType.Id == machineType.Id).ToListAsync();
            var operationsIds = new List<long>();
            foreach (var machineTypeOperation in machineTypeOperations)
            {
                operationsIds.Add(machineTypeOperation.OperationId);
            }

            var machineTypeDTO = new MachineTypeDTO(machineType.Id, machineType.Description, operationsIds);

            return machineTypeDTO;
        }

        [HttpGet("{id}/operations")]
        public async Task<ActionResult<IEnumerable<OperationDTO>>> GetMachineTypeOperations(long id)
        {
            var machineType = await _context.MachineTypes.FindAsync(id);

            if (machineType == null)
            {
                return NotFound();
            }

            var machineTypeOperations = await _context.MachineTypeOperations.Where(mto => mto.MachineType.Id == machineType.Id).ToListAsync();
            var operationsDTOs = new List<OperationDTO>();
            foreach (var machineTypeOperation in machineTypeOperations)
            {
                var operation = _context.Operations.First(o => o.Id == machineTypeOperation.OperationId);
                var operationDTO = new OperationDTO(operation.Id, operation.Duration, new List<long>());
                operationsDTOs.Add(operationDTO);
            }

            return operationsDTOs;
        }

        [HttpGet("{id}/machines")]
        public async Task<ActionResult<IEnumerable<MachineDTO>>> GetMachineTypeMachines(long id)
        {
            var machineType = await _context.MachineTypes.FindAsync(id);

            if (machineType == null)
            {
                return NotFound();
            }

            var machines = await _context.Machines.Where(m => m.MachineTypeId == id).ToListAsync();
            var machinesDTOs = new List<MachineDTO>();
            foreach (var machine in machines)
            {
                machinesDTOs.Add(new MachineDTO(machine.Id, machine.Position, machine.MachineTypeId, machine.ProductionLineId, machine.IsActive));
            }

            return machinesDTOs;
        }

        [HttpPost()]
        [EnableCors("IT3Client")]
        public async Task<ActionResult<Machine>> PostMachineType(MachineTypeDTO item)
        {
            MachineType machineType = new MachineType { Description = item.Description };

            List<Operation> operations = _context.Operations.Where(o => item.OperationsIds.Contains(o.Id)).ToList();
            if (operations.Count() == 0)
            {
                return BadRequest();
            }
            List<MachineTypeOperation> machineTypeOperations = new List<MachineTypeOperation>();
            foreach (var operation in operations)
            {
                var machineTypeOperation = new MachineTypeOperation(operation, machineType);
                machineTypeOperations.Add(machineTypeOperation);
                _context.MachineTypeOperations.Add(machineTypeOperation);
            }

            _context.MachineTypes.Add(machineType);
            await _context.SaveChangesAsync();
            item.Id = machineType.Id;

            return CreatedAtAction(nameof(GetMachineType), new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutMachinesType(long id, OperationsList operationsIds)
        {
            var machineType = _context.MachineTypes.First(mt => mt.Id == id);
            if (machineType == null)
            {
                return BadRequest();
            }

            // 1. Delete all old
            var machineTypeOperations = await _context.MachineTypeOperations.Where(mto => mto.MachineTypeId == id).ToListAsync();
            foreach (var machineTypeOperation in machineTypeOperations)
            {
                _context.MachineTypeOperations.Remove(machineTypeOperation);
            }
            await _context.SaveChangesAsync();

            // 2. Add new
            var operations = await _context.Operations.Where(o => operationsIds.Ids.Contains(o.Id)).ToListAsync();
            foreach (var operation in operations)
            {
                var machineTypeOperation = new MachineTypeOperation(operation, machineType);
                _context.MachineTypeOperations.Add(machineTypeOperation);
            }
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // [HttpPut("{id}")]
        // public async Task<IActionResult> PutMachinesType(long id, List<long> operationsIds)
        // {
        //     var machineType = _context.MachineTypes.First(mt => mt.Id == id);
        //     if (machineType == null)
        //     {
        //         return BadRequest();
        //     }

        //     // 1. Delete all old
        //     var machineTypeOperations = await _context.MachineTypeOperations.Where(mto => mto.MachineTypeId == id).ToListAsync();
        //     foreach (var machineTypeOperation in machineTypeOperations)
        //     {
        //         _context.MachineTypeOperations.Remove(machineTypeOperation);
        //     }
        //     await _context.SaveChangesAsync();

        //     // 2. Add new
        //     var operations = await _context.Operations.Where(o => operationsIds.Contains(o.Id)).ToListAsync();
        //     foreach (var operation in operations)
        //     {
        //         var machineTypeOperation = new MachineTypeOperation(operation, machineType);
        //         _context.MachineTypeOperations.Add(machineTypeOperation);
        //     }
        //     await _context.SaveChangesAsync();

        //     return NoContent();
        // }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMachineType(long id)
        {
            var machineTypeItem = await _context.MachineTypes.FindAsync(id);

            if (machineTypeItem == null)
            {
                return NotFound();
            }

            _context.MachineTypes.Remove(machineTypeItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
